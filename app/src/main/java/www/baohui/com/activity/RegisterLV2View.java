package www.baohui.com.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import www.baohui.com.R;
import www.baohui.com.model.req.JsonLoginObj;
import www.baohui.com.model.req.JsonRegisterObj;
import www.baohui.com.model.res.JsonRegisterValue;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseAppCompatActivity;
import www.baohui.com.service.MAlertDialog;
import www.baohui.com.service.Util;

/**
 * page
 * 註冊頁面2(微信)
 */

public class RegisterLV2View extends BaseAppCompatActivity {
    private Context mContext;
    private RelativeLayout view;
    private RelativeLayout viewClause;
    private TextView clause;
    private EditText appId;
    private EditText appSecret;
    private EditText mchid;
    private EditText key;
    private CheckBox clauseCb;
    private Button ok;

    private JsonRegisterValue registerData;

    private WebView webView;
    private String url;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_lv2_view);

        findView();
        initData();
        onListener();

    }

    private void initData() {
        mContext = this;
        setClauseText();
        registerData = new Gson().fromJson(getIntent().getStringExtra("RegisterLV2View"),new TypeToken<JsonRegisterValue>() {}.getType());

        String appIds = registerData.getaPPID();
        if(appIds != null && !appIds.equals("")){
            appId.setText(appIds);
        }

        String appSecrets = registerData.getaPPSECRET();
        if(appSecrets != null && !appSecrets.equals("")){
            appSecret.setText(appSecrets);
        }

        String mchids = registerData.getmCHID();
        if(mchids != null && !mchids.equals("")){
            mchid.setText(mchids);
        }

        String keys = registerData.getwECHATKEY();
        if(keys != null && !keys.equals("")){
            key.setText(keys);
        }
    }

    private void callRegisterAPI(JsonRegisterValue o) {
        showProgress();
        JsonRegisterObj obj = new JsonRegisterObj();
        obj.setRegisterData(o);
        API.register(this,obj);
    }

    private void callLoginAPI() {
        showProgress();
        JsonLoginObj obj = new JsonLoginObj();
        obj.setAccount(registerData.getAccount());
        obj.setPassword(registerData.getPassword());
        API.login(this,obj);
    }

    private void setClauseText() {
        SpannableString s = new SpannableString(getResources().getString(R.string.r_page_clause_lv2_s));
        s.setSpan(new UnderlineSpan(), 0, s.length(), 0);
        clause.setText(s);
        url = API.policiesTerms();
        webView.setWebChromeClient(webChromeClient);
        webView.setWebViewClient(webViewClient);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setDisplayZoomControls(false);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDefaultTextEncodingName("utf-8");
        webView.loadUrl(url);
    }

    private void findView() {
        clause = (TextView) findViewById(R.id.r_clause);
        view = (RelativeLayout) findViewById(R.id.r_view);
        viewClause = (RelativeLayout) findViewById(R.id.r_view_clause);
        appId = (EditText) findViewById(R.id.r_et_app_id);
        appSecret = (EditText) findViewById(R.id.r_et_app_secret);
        mchid = (EditText) findViewById(R.id.r_et_mchid);
        key = (EditText) findViewById(R.id.r_et_key);
        clauseCb = (CheckBox) findViewById(R.id.r_cb_clause);
        ok = (Button) findViewById(R.id.r_b_register_ok);
        webView = (WebView) findViewById(R.id.webView);
    }

    private void onListener() {
        clause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                view.setVisibility(View.GONE);
                viewClause.setVisibility(View.VISIBLE);
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonRegisterValue obj = check();
                if (obj != null) {
                    callRegisterAPI(obj);
                }
            }
        });
    }

    private JsonRegisterValue check() {
        String appId_s = appId.getText().toString();
        String appSecret_s = appSecret.getText().toString();
        String mchid_s = mchid.getText().toString();
        String key_s = key.getText().toString();

        if (appId_s != null && appId_s.equals("")) {
            showDialog(getString(R.string.please_in_key_s) + appId.getHint().toString());
            return null;
        }

        if (appSecret_s != null && appSecret_s.equals("")) {
            showDialog(getString(R.string.please_in_key_s) + appSecret.getHint().toString());
            return null;
        }

        if (mchid_s != null && mchid_s.equals("")) {
            showDialog(getString(R.string.please_in_key_s) + mchid.getHint().toString());
            return null;
        }

        if (key_s != null && key_s.equals("")) {
            showDialog(getString(R.string.please_in_key_s) + key.getHint().toString());
            return null;
        }

        if(!clauseCb.isChecked()){
            showDialog(clause.getText().toString());
            return null;
        }
        registerData.setaPPID(appId_s);
        registerData.setaPPSECRET(appSecret_s);
        registerData.setmCHID(mchid_s);
        registerData.setwECHATKEY(key_s);

        return registerData;

    }

    private void goToNext() {
        final MAlertDialog mAlertDialog = new MAlertDialog(this);
        mAlertDialog.setMsg(getString(R.string.register_ok_s));
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.dismiss();
                callLoginAPI();
            }
        });
        mAlertDialog.show();
    }

    private void goToDeviceListView() {
        Intent intent = new Intent();
        intent.setClass(mContext, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    WebChromeClient webChromeClient = new WebChromeClient() {

        @Override
        public void onProgressChanged(WebView view, int newProgress) {
            super.onProgressChanged(view, newProgress);
        }
    };

    WebViewClient webViewClient = new WebViewClient() {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
        }

        @Override
        public void onLoadResource(WebView view, String url) {
            super.onLoadResource(view, url);
        }

    };

    @Override
    public void onBackPressed() {
        if(viewClause.getVisibility() == View.VISIBLE){
            view.setVisibility(View.VISIBLE);
            viewClause.setVisibility(View.GONE);
        } else {

            String appId_s = appId.getText().toString();
            String appSecret_s = appSecret.getText().toString();
            String mchid_s = mchid.getText().toString();
            String key_s = key.getText().toString();

            registerData.setaPPID(appId_s);
            registerData.setaPPSECRET(appSecret_s);
            registerData.setmCHID(mchid_s);
            registerData.setwECHATKEY(key_s);

            Intent intent = new Intent();
            intent.putExtra("RegisterLV2View",new Gson().toJson(registerData).toString());
            setResult(RESULT_OK,intent);
            super.onBackPressed();
            overridePendingTransition(0, 0);
        }
    }

    @Override
    public void onPostExecute(Object result , int missionCode) {
        dismissProgress();
        if(!ckeckError(result)){
            // 檢驗基礎傳輸錯誤
            return ;
        }

        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 1){
            // register
            goToNext();
        }

        if(missionCode == 2){
            // callLogin
            String token = (String) data.getValue();
            API.setToken(token);
            getSharedPreferences(API.packageName,0).edit().putString("TOKEN", Util.base64Encode(token)).commit();
            goToDeviceListView();
        }

    }
}
