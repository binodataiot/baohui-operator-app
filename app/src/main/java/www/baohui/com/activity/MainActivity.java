package www.baohui.com.activity;

import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.TextView;

import www.baohui.com.R;
import www.baohui.com.combo.ComboListView;
import www.baohui.com.device.DeviceListView;
import www.baohui.com.member.AccountDetailedView;
import www.baohui.com.service.BaseAppCompatActivity;
import www.baohui.com.service.MAlertDialog;

/**
 * page
 * 登入後主頁面架構
 */

public class MainActivity extends BaseAppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private MainActivity mContext;
    private TextView headerTitle;
    private Button headerBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        findView();
        initData();
        onListener();
    }

    private void initData() {
        mContext = this;
        headerTitle.setText(getResources().getString(R.string.group_list_s));
        initFragment(new DeviceListView(mContext));
    }

    private void findView() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final View navHeaderView = navigationView.getHeaderView(0);
        headerTitle = (TextView) navHeaderView.findViewById(R.id.header_title);
        headerBack = (Button) navHeaderView.findViewById(R.id.back);

        navHeaderView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                float y = navHeaderView.getHeight()/2;
                headerBack.setY(y);
                headerTitle.setY(y);
                navHeaderView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
            }
        });

    }

    private void onListener() {
        headerBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (getSupportFragmentManager().getBackStackEntryCount() != 0) {
                getSupportFragmentManager().popBackStack();
            } else {
                final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
                mAlertDialog.setMsg(getString(R.string.quit_s));
                mAlertDialog.showCancel(true);
                mAlertDialog.setOkOnListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mAlertDialog.dismiss();
                        MainActivity.super.onBackPressed();
                    }
                });
                mAlertDialog.show();
            }
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.group_list_s) {
            initFragment(new DeviceListView(mContext));
            headerTitle.setText(getResources().getString(R.string.group_list_s));
        } else if (id == R.id.combo_setting_s) {
            initFragment(new ComboListView(mContext));
            headerTitle.setText(getResources().getString(R.string.combo_setting_s));
        } else if (id == R.id.account_setting_s) {
            initFragment(new AccountDetailedView(mContext));
            headerTitle.setText(getResources().getString(R.string.account_setting_s));
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        //super.onSaveInstanceState(outState);
    }

    // 初始化Fragment
    public void initFragment(Fragment fragment) {
        changeFragment(fragment, true);
    }

    // 切換Fragment
    public void changeFragment(Fragment fragment) {
        changeFragment(fragment, false);
    }

    private void changeFragment( Fragment fragment , boolean init) {
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.content_frame, fragment);
        if (!init) {
            ft.addToBackStack(null);
        }
        ft.commit();
    }
}