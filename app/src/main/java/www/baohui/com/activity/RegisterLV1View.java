package www.baohui.com.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import www.baohui.com.R;
import www.baohui.com.model.res.JsonRegisterValue;
import www.baohui.com.service.BaseAppCompatActivity;

/**
 * page
 * 註冊頁面1
 */
public class RegisterLV1View extends BaseAppCompatActivity {
    private Context mContext;
    private Button next;
    private EditText storeNo;
    private EditText phone;
    private EditText account;
    private EditText password;
    private EditText passwordCk;

    private JsonRegisterValue registerData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_lv1_view);

        findView();
        initData();
        onListener();

    }

    private void initData() {
        mContext = this;
    }

    private void onListener() {
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                JsonRegisterValue obj = check();
                if(registerData != null){
                    obj.setaPPID(registerData.getaPPID());
                    obj.setaPPSECRET(registerData.getaPPSECRET());
                    obj.setmCHID(registerData.getmCHID());
                    obj.setwECHATKEY(registerData.getwECHATKEY());
                }
                if(obj != null){
                    Intent intent = new Intent();
                    intent.setClass(mContext, RegisterLV2View.class);
                    intent.putExtra("RegisterLV2View",new Gson().toJson(obj).toString());
                    startActivityForResult(intent,99);
                    overridePendingTransition(0, 0);
                }
            }
        });
    }



    private void findView() {
        next = (Button) findViewById(R.id.r_b_next);
        storeNo = (EditText) findViewById(R.id.r_et_store_no);
        phone = (EditText) findViewById(R.id.r_et_phone);
        account = (EditText) findViewById(R.id.r_et_account);
        password = (EditText) findViewById(R.id.r_et_password);
        passwordCk = (EditText) findViewById(R.id.r_et_password_ck);
    }

    private JsonRegisterValue check() {
        String storeNo_s = storeNo.getText().toString();
        String phone_s = phone.getText().toString();
        String account_s = account.getText().toString();
        String password_s = password.getText().toString();
        String passwordCk_s = passwordCk.getText().toString();

        if (storeNo_s != null && storeNo_s.equals("")) {
            showDialog(getString(R.string.please_in_key_s) + storeNo.getHint().toString());
            return null;
        }

        if (phone_s != null && phone_s.equals("")) {
            showDialog(getString(R.string.please_in_key_s) + phone.getHint().toString());
            return null;
        }

        if (account_s != null && account_s.equals("")) {
            showDialog(getString(R.string.please_in_key_s) + account.getHint().toString());
            return null;
        }
        if (password_s != null && password_s.equals("")) {
            showDialog(getString(R.string.please_in_key_s) + password.getHint().toString());
            return null;
        }

        if(!password_s.equals(passwordCk_s)){
            showDialog(getString(R.string.please_in_key_s) + passwordCk.getHint().toString());
            return null;
        }

        if (passwordCk_s != null && passwordCk_s.equals("")) {
            showDialog(getString(R.string.please_in_key_s) + passwordCk.getHint().toString());
            return null;
        }

        JsonRegisterValue obj = new JsonRegisterValue();
        obj.setName(storeNo_s);
        obj.setPhoneNumber(phone_s);
        obj.setAccount(account_s);
        obj.setPassword(password_s);
        return obj;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch(requestCode){
            case 99:
                registerData = new Gson().fromJson(data.getStringExtra("RegisterLV2View"),new TypeToken<JsonRegisterValue>() {}.getType());
                break;
        }
    }
}