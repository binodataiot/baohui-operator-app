package www.baohui.com.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import www.baohui.com.R;
import www.baohui.com.model.req.JsonLoginObj;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseAppCompatActivity;
import www.baohui.com.service.Util;

/**
 * page
 * 登入頁面
 */
public class LoginView extends BaseAppCompatActivity {
    private Context mContext;

    private EditText etAccount;
    private EditText etPassword;
    private Button login;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_view);

        findView();
        initData();
        onListener();
    }

    private void findView() {
        etAccount = (EditText) findViewById(R.id.r_et_account);
        etPassword = (EditText) findViewById(R.id.r_et_password);
        login = (Button) findViewById(R.id.r_b_login);
    }

    private void initData() {
        mContext = this;
    }

    private void onListener() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!check()){
                    return ;
                }
                callAPI();
            }
        });
    }

    private boolean check() {
        String account = etAccount.getText().toString();
        if (account == null || account.equals("")) {
            showDialog(getResources().getString(R.string.error_account_s));
            return false;
        }

        String password = etPassword.getText().toString();
        if (password == null || password.equals("")) {
            showDialog(getResources().getString(R.string.error_password_s));
            return false;
        }
        return true ;
    }

    private void callAPI() {
        showProgress();
        JsonLoginObj obj = new JsonLoginObj();
        obj.setAccount(etAccount.getText().toString());
        obj.setPassword(etPassword.getText().toString());
        API.login(this,obj);
    }

    private void goToNext() {
        Intent intent = new Intent();
        intent.setClass(mContext, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }

    @Override
    public void onPostExecute(Object result , int missionCode) {
        dismissProgress();
        if(!ckeckError(result)){
            // 檢驗基礎傳輸錯誤
            return ;
        }
        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 2){
            String token = (String) data.getValue();
            API.setToken(token);
            getSharedPreferences(API.packageName,0).edit().putString("TOKEN", Util.base64Encode(token)).commit();
            goToNext();
        }
    }
}
