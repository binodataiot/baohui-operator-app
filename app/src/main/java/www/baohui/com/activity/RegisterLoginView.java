package www.baohui.com.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import www.baohui.com.R;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseAppCompatActivity;
import www.baohui.com.service.Util;

/**
 *  page
 *  註冊登入頁面
 */

public class RegisterLoginView extends BaseAppCompatActivity {
    private Context mContext;
    private Button login;
    private Button register;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_login_view);

        findView();
        initData();
        onListener();
    }

    private void initData() {
        mContext = this ;
        String host = getSharedPreferences(API.packageName,0).getString("HOST", getString(R.string.host_s));
        API.setHost(host);
        String token = getSharedPreferences(API.packageName,0).getString("TOKEN","");
        if(!token.equals("")){
            API.setToken(Util.base64Decode(token));
            goToMain();
        }
    }

    private void goToMain() {
        Intent intent = new Intent();
        intent.setClass(mContext, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
        overridePendingTransition(0, 0);
    }

    private void onListener() {
        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext,LoginView.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(mContext,RegisterLV1View.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });
    }

    private void findView() {
        login = (Button) findViewById(R.id.lr_b_login);
        register = (Button) findViewById(R.id.lr_b_register);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, 0);
    }
}
