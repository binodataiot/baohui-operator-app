package www.baohui.com.member;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import one.customutil.GsonAsyncTask;
import www.baohui.com.R;
import www.baohui.com.activity.MainActivity;
import www.baohui.com.activity.RegisterLoginView;
import www.baohui.com.model.req.JsonOperatorPasswordUpdateObj;
import www.baohui.com.model.req.JsonOperatorWechatUpdateObj;
import www.baohui.com.model.res.JsonRegisterValue;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseFragment;
import www.baohui.com.service.MAlertDialog;

/**
 * page
 * 帳號設定頁面
 */
@SuppressLint("ValidFragment")
public class AccountDetailedView extends BaseFragment implements GsonAsyncTask.onPostExecute {
    private MainActivity mContext;
    private JsonRegisterValue value;
    private View view;
    private Button loginOut;
    private ImageView passwordRe;
    private ImageView wechatRe;
    private TextView account;
    private TextView name;
    private TextView appId;
    private TextView appSecret;
    private TextView mchid;
    private TextView key;

    public AccountDetailedView (MainActivity context) {
        mContext = context ;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle( getResources().getString(R.string.account_setting_s));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container , Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.account_detailed_view, container, false);
        findView();
        initData();
        onListener();
        return this.view;
    }

    private void onListener() {
        loginOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callLoginDialog();
            }
        });
        passwordRe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callRePasswordDialog();
            }
        });
        wechatRe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callReWecathDialog();
            }
        });
    }

    private void findView() {
        loginOut = (Button) this.view.findViewById(R.id.login_out);
        passwordRe = (ImageView) this.view.findViewById(R.id.s_password_re);
        wechatRe = (ImageView) this.view.findViewById(R.id.s_wechat_re);
        account = (TextView) this.view.findViewById(R.id.ad_account);
        name = (TextView) this.view.findViewById(R.id.ad_name);
        appId = (TextView) this.view.findViewById(R.id.ad_appid);
        appSecret = (TextView) this.view.findViewById(R.id.ad_app_secret);
        mchid = (TextView) this.view.findViewById(R.id.ad_mchid);
        key = (TextView) this.view.findViewById(R.id.ad_key);
    }

    private void initData() {
        callAPI();
    }

    /**
     * API
     * 取得帳號資訊
     */
    private void callAPI() {
        mContext.showProgress();
        API.operatorDetail(this);
    }

    /**
     *API
     * 變更密碼
     */
    private void callOperatorPasswordUpdateAPI(String old , String news) {
        mContext.showProgress();
        JsonOperatorPasswordUpdateObj obj = new JsonOperatorPasswordUpdateObj();
        obj.setOldPassword(old);
        obj.setNewPassword(news);
        API.operatorPasswordUpdate(this,obj);
    }

    /**
     *API
     * 更新微信資料
     */
    private void callOperatorWechatUpdateAPI(String appid , String appsecret , String chid ,String key){
        mContext.showProgress();
        JsonOperatorWechatUpdateObj obj = new JsonOperatorWechatUpdateObj();
        obj.setAPPID(appid);
        obj.setAPPSECRET(appsecret);
        obj.setMCHID(chid);
        obj.setWECHATKEY(key);
        API.operatorWechatUpdate(this,obj);
    }

    private void goLoginOut() {
        Intent intent = new Intent();
        intent.setClass(mContext, RegisterLoginView.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        mContext.finish();
        mContext.overridePendingTransition(0, 0);
    }

    private void callRePasswordDialog() {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setMsg(getResources().getString(R.string.user_password_ch_s));
        mAlertDialog.setEditHint(getResources().getString(R.string.user_password_s));
        mAlertDialog.setEdit4Hint(getResources().getString(R.string.user_new_password_s));
        mAlertDialog.setEditType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mAlertDialog.setEdit4Type(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = mAlertDialog.getEditText();
                if (value != null && value.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return;
                }

                String value4 = mAlertDialog.getEdit4Text();
                if (value4 != null && value4.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return;
                }

                callOperatorPasswordUpdateAPI(value , value4);
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void callReWecathDialog() {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setMsg(getResources().getString(R.string.user_update_wechat_data_s));
        mAlertDialog.setEditHint(getResources().getString(R.string.r_page_register_app_id));
        mAlertDialog.setEdit2Hint(getResources().getString(R.string.r_page_register_app_secret));
        mAlertDialog.setEdit3Hint(getResources().getString(R.string.r_page_register_mchid));
        mAlertDialog.setEdit4Hint(getResources().getString(R.string.r_page_register_key));
        mAlertDialog.setEditText(value.getaPPID());
        mAlertDialog.setEdit2Text(value.getaPPSECRET());
        mAlertDialog.setEdit3Text(value.getmCHID());
        mAlertDialog.setEdit4Text(value.getwECHATKEY());
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = mAlertDialog.getEditText();
                if (value != null && value.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return;
                }

                String value2 = mAlertDialog.getEdit2Text();
                if (value2 != null && value2.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return;
                }

                String value3 = mAlertDialog.getEdit3Text();
                if (value3 != null && value3.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return;
                }

                String value4 = mAlertDialog.getEdit4Text();
                if (value4 != null && value4.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return;
                }


                callOperatorWechatUpdateAPI(value ,value2,value3, value4);
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void callLoginDialog() {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setMsg(getString(R.string.login_out_ok_s));
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mAlertDialog.dismiss();
                API.setToken("");
                mContext.getSharedPreferences(API.packageName,0).edit().putString("TOKEN","").commit();
                goLoginOut();
            }
        });
        mAlertDialog.show();
    }

    private void setShowView(JsonRegisterValue value) {
        this.value = value ;
        account.setText(value.getAccount());
        name.setText(value.getName());
        appId.setText(value.getaPPID());
        appSecret.setText(value.getaPPSECRET());
        mchid.setText(value.getmCHID());
        key.setText(value.getwECHATKEY());
    }

    /**
     *API回傳
     */
    @Override
    public void onPostExecute(Object result , int missionCode) {
        mContext.dismissProgress();
        if(!ckeckError(result,mContext)){
            // 檢驗基礎傳輸錯誤
            return ;
        }
        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 26){
            // operatorDetail
            JsonRegisterValue value = (JsonRegisterValue) data.getValue();
            setShowView(value);
        }

        if(missionCode == 27){
            // operatorWechatUpdate
            showDialog(mContext,getString(R.string.success_s));
            callAPI();
        }

        if(missionCode == 23){
            // operatorPasswordUpdate
            showDialog(mContext,getString(R.string.pwd_change_ok_s));
        }
    }
}
