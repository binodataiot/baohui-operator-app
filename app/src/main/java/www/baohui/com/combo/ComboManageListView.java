package www.baohui.com.combo;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import www.baohui.com.activity.MainActivity;
import www.baohui.com.R;
import www.baohui.com.adapter.ComboManageListAdapter;
import www.baohui.com.model.req.JsonDeviceComboDeleteObj;
import www.baohui.com.model.req.JsonDeviceComboListObj;
import www.baohui.com.model.res.JsonComboListValue;
import www.baohui.com.model.res.JsonDeviceListValue;
import www.baohui.com.model.res.JsonListObj;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseFragment;
import www.baohui.com.service.MAlertDialog;

/**
 * page
 * 套餐管理頁面
 */
@SuppressLint("ValidFragment")
public class ComboManageListView extends BaseFragment {
    private MainActivity mContext;
    private View view;
    private ComboManageListAdapter adapter;

    private ListView listView;
    private ImageView imageView;
    private JsonDeviceListValue posValue;

    private  List<JsonComboListValue> value;

    public ComboManageListView(MainActivity context , JsonDeviceListValue value) {
        mContext = context ;
        this.posValue = value ;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getResources().getString(R.string.gdc_page_combo_manage_s));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.combo_manage_list_view, container, false);
        findView();
        initData();
        onListener();
        return this.view;
    }

    private void findView() {
        listView = (ListView) this.view.findViewById(R.id.listView);
        imageView = (ImageView) this.view.findViewById(R.id.imageView);
    }

    private void initData() {
        callAPI();
    }

    /**
     * API
     * 套餐組合列表
     */
    private void callAPI() {
        mContext.showProgress();
        JsonDeviceComboListObj obj = new JsonDeviceComboListObj();
        obj.setDeviceId(posValue.getDeviceId());
        API.deviceComboList(this,obj);
    }

    /**
     *API
     * 刪除套餐組合
     */
    private void callDeviceComboDeleteAPI(JsonDeviceComboDeleteObj obj) {
        mContext.showProgress();
        API.deviceComboDelete(this,obj);
    }

    private void onListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JsonComboListValue posValue = (JsonComboListValue) adapter.getItem(position);
                callDeleteDialog(posValue);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_combo_in_add_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_add:
                mContext.changeFragment(new ComboInListView(mContext , posValue , this.value));
                return true;
            default:
                break;
        }
        return false;
    }

    private void callDeleteDialog(final JsonComboListValue value) {
        String name = value.getName();
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setMsg(getResources().getString(R.string.gdc_page_ch_delete_s) + name + "?");
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<String> combos  = new ArrayList<String>() ;
                combos.add(value.getComboId().toString());
                JsonDeviceComboDeleteObj obj = new JsonDeviceComboDeleteObj();
                obj.setDeviceId(posValue.getDeviceId());
                obj.setCombo(value.getComboId().toString());
                callDeviceComboDeleteAPI(obj);
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void setListView(List<JsonComboListValue> value) {
        this.value = value ;
        adapter = new ComboManageListAdapter(mContext , value);
        listView.setAdapter(adapter);
        imageView.setVisibility((adapter.getCount() != 0) ? View.GONE : View.VISIBLE);
    }

    /**
     *API回傳
     */
    @Override
    public void onPostExecute(Object result , int missionCode) {
        mContext.dismissProgress();
        if(!ckeckError(result,mContext)){
            // 檢驗基礎傳輸錯誤
            return ;
        }
        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 15){
            // deviceComboList
            JsonListObj jsonListObj = (JsonListObj) data.getValue();
            setListView(jsonListObj.getValue());
        }

        if(missionCode == 17){
            // deviceComboDelete
            showDialog(mContext,getString(R.string.success_s));
            callAPI();
        }
    }



}
