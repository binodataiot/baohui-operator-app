package www.baohui.com.combo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.List;

import www.baohui.com.activity.MainActivity;
import www.baohui.com.R;
import www.baohui.com.adapter.ComboInListAdapter;
import www.baohui.com.model.req.JsonDeviceComboApplyObj;
import www.baohui.com.model.res.JsonComboListValue;
import www.baohui.com.model.res.JsonDeviceListValue;
import www.baohui.com.model.res.JsonListObj;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseFragment;
import www.baohui.com.service.Util;

/**
 * page
 *  加入套餐頁面
 */
@SuppressLint("ValidFragment")
public class ComboInListView extends BaseFragment {
    private MainActivity mContext;
    private View view;
    private ComboInListAdapter adapter;

    private ListView listView;
    private ImageView imageView;
    private Button ok;

    private JsonDeviceListValue posValue;
    private List<JsonComboListValue> value;
    private List<JsonComboListValue> comboListValues;

    public ComboInListView(MainActivity context, JsonDeviceListValue value, List<JsonComboListValue> comboListValues) {
        mContext = context;
        this.posValue = value;
        this.comboListValues = comboListValues;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getResources().getString(R.string.gdc_page_combo_in_add_s));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.combo_in_list_view, container, false);
        findView();
        initData();
        onListener();
        return this.view;
    }

    private void findView() {
        listView = (ListView) this.view.findViewById(R.id.listView);
        imageView = (ImageView) this.view.findViewById(R.id.imageView);
        ok = (Button) this.view.findViewById(R.id.ok);
    }

    private void initData() {
        callAPI();
    }

    /**
     * API
     * 套餐列表
     */
    private void callAPI() {
        mContext.showProgress();
        API.comboList(this);
    }

    private void callDeviceComboApplyAPI(List<Integer> combos) {
        JsonDeviceComboApplyObj obj = new JsonDeviceComboApplyObj();
        obj.setDeviceId(posValue.getDeviceId().toString());
        obj.setCombo(combos);
        mContext.showProgress();
        API.deviceComboApply(this,obj);
    }

    private void onListener() {

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<Integer> combos  = new ArrayList<Integer>() ;
                for (int i = 0; i < value.size(); i++) {
                    JsonComboListValue data = value.get(i);
                    if(data.isCheckBox()){
                        combos.add(data.getComboId());
                    }
                }
                if(combos.size() != 0){
                    callDeviceComboApplyAPI(combos);
                } else {
                    getFragmentManager().popBackStack();
                }
            }
        });

    }

    private void setListView(List<JsonComboListValue> value) {
        // 過濾現有機台設定套餐
        if ((this.comboListValues != null && this.comboListValues.size() != 0) && (value != null && value.size() != 0)) {
            this.value = Util.repeatJsonComboListValue(comboListValues , value);
        } else {
            this.value = value;
        }
        adapter = new ComboInListAdapter(mContext,this.value);
        listView.setAdapter(adapter);
        imageView.setVisibility((adapter.getCount() != 0) ? View.GONE : View.VISIBLE);
    }

    /**
     *API回傳
     */
    @Override
    public void onPostExecute(Object result , int missionCode) {
        mContext.dismissProgress();
        if(!ckeckError(result,mContext)){
            // 檢驗基礎傳輸錯誤
            return ;
        }
        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 12){
            // comboList
            JsonListObj jsonListObj = (JsonListObj) data.getValue();
            setListView(jsonListObj.getValue());
        }

        if(missionCode == 16){
            // deviceComboApply
            showDialog(mContext,getString(R.string.success_s));
            getFragmentManager().popBackStack();
        }
    }
}