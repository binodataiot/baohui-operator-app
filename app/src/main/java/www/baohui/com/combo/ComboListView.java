package www.baohui.com.combo;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.List;

import www.baohui.com.activity.MainActivity;
import www.baohui.com.R;
import www.baohui.com.adapter.ComboListAdapter;
import www.baohui.com.model.req.JsonComboCreateObj;
import www.baohui.com.model.req.JsonComboDeleteObj;
import www.baohui.com.model.res.JsonComboListValue;
import www.baohui.com.model.res.JsonListObj;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseFragment;
import www.baohui.com.service.MAlertDialog;
import www.baohui.com.service.Util;

/**
 * page
 * 套餐設定頁面
 */
@SuppressLint("ValidFragment")
public class ComboListView extends BaseFragment {
    private MainActivity mContext;
    private View view;
    private ComboListAdapter adapter;

    private ListView listView;
    private ImageView imageView;
    private  List<JsonComboListValue> value;

    public ComboListView(MainActivity context) {
        mContext = context ;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getResources().getString(R.string.combo_setting_s));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.combo_list_view, container, false);
        findView();
        initData();
        onListener();
        return this.view;
    }

    private void findView() {
        listView = (ListView) this.view.findViewById(R.id.listView);
        imageView = (ImageView) this.view.findViewById(R.id.imageView);
    }

    private void initData() {
        callAPI();
    }

    /**
     * API
     * 套餐列表
     */
    private void callAPI() {
        mContext.showProgress();
        API.comboList(this);
    }

    /**
     *API
     * 建立套餐列表
     */
    private void callCreateAPI(JsonComboCreateObj obj) {
        mContext.showProgress();
        API.comboCreate(this,obj);
    }

    /**
     *API
     * 刪除套餐列表
     */
    private void callDeleteAPI(JsonComboListValue posValue) {
        mContext.showProgress();
        JsonComboDeleteObj obj = new JsonComboDeleteObj();
        obj.setComboId(posValue.getComboId().toString());
        API.comboDelete(this,obj);
    }

    private void onListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JsonComboListValue posValue = (JsonComboListValue) adapter.getItem(position);
                String name = posValue.getName();
                callDeleteDialog(name , posValue);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_combo_add_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_add:
                callAddDialog();
                return true;
            default:
                break;
        }
        return false;
    }

    private void callAddDialog() {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setEditHint(getResources().getString(R.string.gdc_page_name_s));
        mAlertDialog.setEdit2Hint(getResources().getString(R.string.gdc_page_money_s));
        mAlertDialog.setEdit2Type(InputType.TYPE_NUMBER_FLAG_DECIMAL);
//        mAlertDialog.setEdit2KeyListener(DigitsKeyListener.getInstance(".0123456789"));
        mAlertDialog.setEdit3Hint(getResources().getString(R.string.gdc_page_frequency_s));
        mAlertDialog.setEdit3Type(InputType.TYPE_CLASS_PHONE);
        mAlertDialog.setEdit3KeyListener(DigitsKeyListener.getInstance("0123456789"));
        mAlertDialog.setMsg(getResources().getString(R.string.gdc_page_combo_add_s));
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = mAlertDialog.getEditText();
                String value2 = mAlertDialog.getEdit2Text();
                String value3 = mAlertDialog.getEdit3Text();

                if (value != null && value.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return ;
                }

                if (value2 != null && value2.equals("") || !Util.isDECIMAL(value2)) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return ;
                }


                if (value3 != null && value3.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return ;
                }

                JsonComboCreateObj obj = new JsonComboCreateObj();
                obj.setName(value);
                obj.setPrice(value2);
                obj.setRounds(value3);
                callCreateAPI(obj);

                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void callDeleteDialog(String name, final JsonComboListValue posValue) {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setMsg(getResources().getString(R.string.gdc_page_ch_delete_s) + name + "?");
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDeleteAPI(posValue);
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void setListView(List<JsonComboListValue> value) {
        this.value = value;
        adapter = new ComboListAdapter(mContext,this.value);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        imageView.setVisibility((adapter.getCount() != 0) ? View.GONE : View.VISIBLE);
    }
    /**
     *API回傳
     */
    @Override
    public void onPostExecute(Object result , int missionCode) {
        mContext.dismissProgress();
        if(!ckeckError(result,mContext)){
            // 檢驗基礎傳輸錯誤
            return ;
        }
        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 12){
            // comboList
            JsonListObj jsonListObj = (JsonListObj) data.getValue();
            setListView(jsonListObj.getValue());
        }

        if(missionCode == 13){
            // comboCreate
            showDialog(mContext,getString(R.string.success_s));
            callAPI();
        }

        if(missionCode == 14){
            // comboDelete
            showDialog(mContext,getString(R.string.success_s));
            callAPI();
        }

    }

}
