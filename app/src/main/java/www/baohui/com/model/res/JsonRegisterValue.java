package www.baohui.com.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonRegisterValue {
    @SerializedName("Operator")
    @Expose
    private Operator operator;
    @SerializedName("OpAccount")
    @Expose
    private OpAccount opAccount;
    @SerializedName("WeInfo")
    @Expose
    private WeInfo weInfo;

    public JsonRegisterValue() {
        this.operator = new Operator();
        this.weInfo = new WeInfo();
        this.opAccount = new OpAccount();
    }

    public Operator getOperator() {
        return operator;
    }

    public OpAccount getOpAccount() {
        return opAccount;
    }

    public WeInfo getWeInfo() {
        return weInfo;
    }


    public String getName() {
        return this.operator.getName();
    }

    public void setName(String name) {
        this.operator.setName(name);
    }

    public String getPhoneNumber() {
        return this.operator.getPhoneNumber();
    }

    public void setPhoneNumber(String phoneNumber) {
        this.operator.setPhoneNumber(phoneNumber);
    }

    public String getaPPID() {
        return this.weInfo.getaPPID();
    }

    public void setaPPID(String aPPID) {
        this.weInfo.setaPPID(aPPID);
    }

    public String getaPPSECRET() {
        return this.weInfo.getaPPSECRET();
    }

    public void setaPPSECRET(String aPPSECRET) {
        this.weInfo.setaPPSECRET(aPPSECRET);
    }

    public String getmCHID() {
        return this.weInfo.getmCHID();
    }

    public void setmCHID(String mCHID) {
        this.weInfo.setmCHID(mCHID);
    }

    public String getwECHATKEY() {
        return this.weInfo.getwECHATKEY();
    }

    public void setwECHATKEY(String wECHATKEY) {
        this.weInfo.setwECHATKEY(wECHATKEY);
    }

    public String getAccount() {
        return this.opAccount.getAccount();
    }

    public void setAccount(String account) {
        this.opAccount.setAccount(account);
    }

    public String getPassword() {
        return this.opAccount.getPassword();
    }

    public void setPassword(String password) {
        this.opAccount.setPassword(password);
    }

}

class Operator {
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("PhoneNumber")
    @Expose
    private String phoneNumber;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

}

class OpAccount {
    @SerializedName("Account")
    @Expose
    private String account;
    @SerializedName("Password")
    @Expose
    private String password;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}

class WeInfo {
    @SerializedName("APPID")
    @Expose
    private String aPPID;
    @SerializedName("APPSECRET")
    @Expose
    private String aPPSECRET;
    @SerializedName("MCHID")
    @Expose
    private String mCHID;
    @SerializedName("WECHATKEY")
    @Expose
    private String wECHATKEY;

    public String getaPPID() {
        return aPPID;
    }

    public void setaPPID(String aPPID) {
        this.aPPID = aPPID;
    }

    public String getaPPSECRET() {
        return aPPSECRET;
    }

    public void setaPPSECRET(String aPPSECRET) {
        this.aPPSECRET = aPPSECRET;
    }

    public String getmCHID() {
        return mCHID;
    }

    public void setmCHID(String mCHID) {
        this.mCHID = mCHID;
    }

    public String getwECHATKEY() {
        return wECHATKEY;
    }

    public void setwECHATKEY(String wECHATKEY) {
        this.wECHATKEY = wECHATKEY;
    }
}
