package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonDeviceUpdateObj {
    private List<NameValuePair> params;

    public JsonDeviceUpdateObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setDeviceId(String deviceId) {
        params.add(new BasicNameValuePair("DeviceId", deviceId));
    }

    public void setName(String name) {
        params.add(new BasicNameValuePair("Name", name));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
