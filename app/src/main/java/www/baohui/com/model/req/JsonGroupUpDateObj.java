package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonGroupUpDateObj {
    private List<NameValuePair> params;

    public JsonGroupUpDateObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setGroupName(String groupName) {
        params.add(new BasicNameValuePair("GroupName", groupName));
    }

    public void setGroupId(String groupId) {
        params.add(new BasicNameValuePair("GroupId", groupId));
    }

    public void setDeviceName(String deviceName) {
        params.add(new BasicNameValuePair("GroupName", deviceName));
    }

    public void setDeviceId(String deviceId) {
        params.add(new BasicNameValuePair("DeviceId", deviceId));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
