package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonDeviceCheckQRCodeObj {
    private List<NameValuePair> params;

    public JsonDeviceCheckQRCodeObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setSN(String SN) {
        params.add(new BasicNameValuePair("SN", SN));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
