package www.baohui.com.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonOperatorAuthConfirmValue {
    @SerializedName("VerifyResult")
    @Expose
    private Boolean verifyResult;

    public Boolean getVerifyResult() {
        return verifyResult;
    }

    public void setVerifyResult(Boolean verifyResult) {
        this.verifyResult = verifyResult;
    }
}
