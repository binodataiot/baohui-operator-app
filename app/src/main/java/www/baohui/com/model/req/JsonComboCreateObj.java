package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonComboCreateObj {
    private List<NameValuePair> params;

    public JsonComboCreateObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setName(String name) {
        params.add(new BasicNameValuePair("Name", name));
    }

    public void setPrice(String price) {
        params.add(new BasicNameValuePair("Price", price));
    }

    public void setRounds(String rounds) {
        params.add(new BasicNameValuePair("Rounds", rounds));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
