package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonOperatorPasswordUpdateObj {
    private List<NameValuePair> params;

    public JsonOperatorPasswordUpdateObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setOldPassword(String oldPassword) {
        params.add(new BasicNameValuePair("OldPassword", oldPassword));
    }

    public void setNewPassword(String newPassword) {
        params.add(new BasicNameValuePair("NewPassword", newPassword));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
