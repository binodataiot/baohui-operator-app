package www.baohui.com.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonResponseObj <T>{
    @SerializedName("Value")
    @Expose
    private T value;
    @SerializedName("Valid")
    @Expose
    private Boolean valid;
    @SerializedName("ResultMessage")
    @Expose
    private String resultMessage;
    @SerializedName("ResultCode")
    @Expose
    private Integer resultCode;
    @SerializedName("TaskId")
    @Expose
    private String taskId;

    public T getValue() {
        return value;
    }

    public void setValue(T value) {
        this.value = value;
    }

    public Boolean getValid() {
        return valid;
    }

    public void setValid(Boolean valid) {
        this.valid = valid;
    }

    public String getResultMessage() {
        return resultMessage;
    }

    public void setResultMessage(String resultMessage) {
        this.resultMessage = resultMessage;
    }

    public Integer getResultCode() {
        return resultCode;
    }

    public void setResultCode(Integer resultCode) {
        this.resultCode = resultCode;
    }

    public String getTaskId() {
        return taskId;
    }

    public void setTaskId(String taskId) {
        this.taskId = taskId;
    }

}
