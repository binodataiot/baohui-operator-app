package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonMailSendEmailDeviceIdObj {
    private List<NameValuePair> params;
    private String email;
    private String deviceId;

    public JsonMailSendEmailDeviceIdObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setEmail(String email) {
        this.email = email;
        params.add(new BasicNameValuePair("Email", email));
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
        params.add(new BasicNameValuePair("DeviceId", deviceId));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

    public String getEmail() {
        return email;
    }

    public String getDeviceId() {
        return deviceId;
    }
}
