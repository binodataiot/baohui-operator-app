package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonComboDeleteObj {
    private List<NameValuePair> params;

    public JsonComboDeleteObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setComboId(String comboId) {
        params.add(new BasicNameValuePair("ComboId", comboId));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
