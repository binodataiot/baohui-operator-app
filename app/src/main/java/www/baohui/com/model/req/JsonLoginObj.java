package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonLoginObj {
    private List<NameValuePair> params;

    public JsonLoginObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setAccount(String account) {
        params.add(new BasicNameValuePair("Account", account));
    }

    public void setPassword(String password) {
        params.add(new BasicNameValuePair("Password", password));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
