package www.baohui.com.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonDeviceDetailedValue {
    @SerializedName("SN")
    @Expose
    private String sn;
    @SerializedName("BindedDateTime")
    @Expose
    private String bindedDateTime;
    @SerializedName("GroupName")
    @Expose
    private String groupName;
    @SerializedName("DeviceName")
    @Expose
    private String deviceName;
    @SerializedName("IsFail")
    @Expose
    private Boolean isFail;
    @SerializedName("IsReplenished")
    @Expose
    private Boolean isReplenished;
    @SerializedName("IsOperatorBinded")
    @Expose
    private Boolean isOperatorBinded;

    public String getSn() {
        return sn;
    }

    public void setSn(String sn) {
        this.sn = sn;
    }

    public String getBindedDateTime() {
        return bindedDateTime;
    }

    public void setBindedDateTime(String bindedDateTime) {
        this.bindedDateTime = bindedDateTime;
    }

    public String getGroupName() {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public Boolean getFail() {
        return isFail;
    }

    public void setFail(Boolean fail) {
        isFail = fail;
    }

    public Boolean getReplenished() {
        return isReplenished;
    }

    public void setReplenished(Boolean replenished) {
        isReplenished = replenished;
    }

    public Boolean getOperatorBinded() {
        return isOperatorBinded;
    }

    public void setOperatorBinded(Boolean operatorBinded) {
        isOperatorBinded = operatorBinded;
    }
}
