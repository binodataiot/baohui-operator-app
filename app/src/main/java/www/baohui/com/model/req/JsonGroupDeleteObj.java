package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonGroupDeleteObj {
    private List<NameValuePair> params;

    public JsonGroupDeleteObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setGroupId(String groupId) {
        params.add(new BasicNameValuePair("GroupId", groupId));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
