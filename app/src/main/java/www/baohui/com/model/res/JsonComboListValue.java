package www.baohui.com.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonComboListValue {
    @SerializedName("ComboId")
    @Expose
    private Integer comboId;
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("Price")
    @Expose
    private String price;
    @SerializedName("Rounds")
    @Expose
    private String rounds;

    private boolean checkBox;
    private boolean repeatTag;

    public boolean isRepeatTag() {
        return repeatTag;
    }

    public void setRepeatTag(boolean repeatTag) {
        this.repeatTag = repeatTag;
    }

    public boolean isCheckBox() {
        return checkBox;
    }

    public void setCheckBox(boolean checkBox) {
        this.checkBox = checkBox;
    }

    public Integer getComboId() {
        return comboId;
    }

    public void setComboId(Integer comboId) {
        this.comboId = comboId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getRounds() {
        return rounds;
    }

    public void setRounds(String rounds) {
        this.rounds = rounds;
    }
}
