package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonDeviceDetailedObj {
    private List<NameValuePair> params;

    public JsonDeviceDetailedObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setDeviceId(String deviceId) {
        params.add(new BasicNameValuePair("DeviceId", deviceId));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
