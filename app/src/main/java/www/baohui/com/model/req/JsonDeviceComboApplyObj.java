package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonDeviceComboApplyObj {
    private List<NameValuePair> params;

    public JsonDeviceComboApplyObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setDeviceId(String deviceId) {
        params.add(new BasicNameValuePair("DeviceGuid", deviceId));
    }

    public void setCombo(List<Integer> combos) {
        for (int i = 0; i < combos.size(); i++) {
            params.add(new BasicNameValuePair("Combo", combos.get(i).toString()));
        }
    }

    public List<NameValuePair> getParams() {
        System.out.println("params = " + params.toString());
        return params;
    }

}
