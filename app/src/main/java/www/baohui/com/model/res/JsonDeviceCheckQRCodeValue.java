package www.baohui.com.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonDeviceCheckQRCodeValue {
    @SerializedName("IsOperatorBinded")
    @Expose
    private String isOperatorBinded;
    public String getIsOperatorBinded() {
        return isOperatorBinded;
    }

    public void setIsOperatorBinded(String isOperatorBinded) {
        this.isOperatorBinded = isOperatorBinded;
    }
}
