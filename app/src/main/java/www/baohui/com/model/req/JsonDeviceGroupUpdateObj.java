package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonDeviceGroupUpdateObj {
    private List<NameValuePair> params;

    public JsonDeviceGroupUpdateObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setDeviceId(String deviceId) {
        params.add(new BasicNameValuePair("DeviceId", deviceId));
    }

    public void setGroupId(String groupId) {
        params.add(new BasicNameValuePair("GroupId", groupId));
    }


    public List<NameValuePair> getParams() {
        System.out.println("params = " + params.toString());
        return params;
    }

}
