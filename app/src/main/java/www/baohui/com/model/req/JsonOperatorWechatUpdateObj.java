package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonOperatorWechatUpdateObj {
    private List<NameValuePair> params;

    public JsonOperatorWechatUpdateObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setAPPID(String aPPID) {
        params.add(new BasicNameValuePair("APPID", aPPID));
    }

    public void setAPPSECRET(String aPPSECRET) {
        params.add(new BasicNameValuePair("APPSECRET", aPPSECRET));
    }

    public void setMCHID(String mCHID) {
        params.add(new BasicNameValuePair("MCHID", mCHID));
    }

    public void setWECHATKEY(String wECHATKEY) {
        params.add(new BasicNameValuePair("WECHATKEY", wECHATKEY));
    }

    public List<NameValuePair> getParams() {
        return params;
    }
}
