package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonDeviceActiveObj {
    private List<NameValuePair> params;

    public JsonDeviceActiveObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setSN(String sn) {
        params.add(new BasicNameValuePair("SN", sn));
    }

    public void setName(String name) {
        params.add(new BasicNameValuePair("Name", name));
    }

    public void setGroupId(String groupId) {
        params.add(new BasicNameValuePair("GroupId", groupId));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
