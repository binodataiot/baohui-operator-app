package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonGroupCreateObj {
    private List<NameValuePair> params;

    public JsonGroupCreateObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setGroupName(String groupName) {
        params.add(new BasicNameValuePair("GroupName", groupName));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
