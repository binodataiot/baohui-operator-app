package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonDeviceComboDeleteObj {
    private List<NameValuePair> params;

    public JsonDeviceComboDeleteObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setDeviceId(String deviceId) {
        params.add(new BasicNameValuePair("DeviceGuid", deviceId));
    }

    public void setCombo(String combo) {
        params.add(new BasicNameValuePair("Combo", combo));
    }

    public List<NameValuePair> getParams() {
        return params;
    }

}
