package www.baohui.com.model.res;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class JsonDeviceListValue {
    @SerializedName("Name")
    @Expose
    private String name;
    @SerializedName("DeviceId")
    @Expose
    private String deviceId;
    @SerializedName("IsLock")
    @Expose
    private Boolean isLock;
    @SerializedName("IsFail")
    @Expose
    private Boolean isFail;
    @SerializedName("IsReplenished")
    @Expose
    private Boolean isReplenished;
    @SerializedName("DeviceTypeName")
    @Expose
    private String deviceTypeName;
    @SerializedName("GroupId")
    @Expose
    private Integer groupId;

    public Integer getGroupId() {
        return groupId;
    }

    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    public String getDeviceTypeName() {
        return deviceTypeName;
    }

    public void setDeviceTypeName(String deviceTypeName) {
        this.deviceTypeName = deviceTypeName;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public Boolean getLock() {
        return isLock;
    }

    public void setLock(Boolean lock) {
        isLock = lock;
    }

    public Boolean getFail() {
        return isFail;
    }

    public void setFail(Boolean fail) {
        isFail = fail;
    }

    public Boolean getReplenished() {
        return isReplenished;
    }

    public void setReplenished(Boolean replenished) {
        isReplenished = replenished;
    }
}
