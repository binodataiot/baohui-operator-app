package www.baohui.com.model.req;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.List;

public class JsonOperatorAuthConfirmObj {
    private List<NameValuePair> params;

    public JsonOperatorAuthConfirmObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setPassword(String password) {
        params.add(new BasicNameValuePair("Password", password));
    }

    public List<NameValuePair> getParams() {
        return params;
    }
}
