package www.baohui.com.model.req;

import com.google.gson.Gson;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import www.baohui.com.model.res.JsonRegisterValue;

public class JsonRegisterObj {
    private List<NameValuePair> params;

    public JsonRegisterObj() {
        params = new ArrayList<NameValuePair>();
    }

    public void setRegisterData(JsonRegisterValue obj) {
        try {
            params = nameValueJson(new JSONObject(new Gson().toJson(obj).toString()),"");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public List<NameValuePair> getParams() {
        return params;
    }

    protected static List<NameValuePair> nameValueJson(JSONObject aObj, String akeyprefix) {
        Iterator<?> keys = aObj.keys();
        int index = 0;
        String keyfordata = null;
        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            if (akeyprefix.isEmpty()) {
                keyfordata = key;
            } else {
                keyfordata = akeyprefix + "[" + key + "]";
            }
            try {
                if (aObj.get(key) instanceof JSONObject) {
                    List<NameValuePair> rnameValuePairs = nameValueJson((JSONObject) aObj.get(key), keyfordata);
                    nameValuePairs.addAll(rnameValuePairs);
                } else if (aObj.get(key) instanceof JSONArray) {
                    JSONArray jarray = (JSONArray) aObj.get(key);
                    for (int i = 0; i < jarray.length(); i++) {
                        JSONObject row = jarray.optJSONObject(i);
                        if (row != null) {
                            List<NameValuePair> rnameValuePairs = nameValueJson(row, keyfordata + "[" + i + "]");
                            nameValuePairs.addAll(rnameValuePairs);
                        } else {
                            nameValuePairs.add(new BasicNameValuePair(key + '[' + i + ']', jarray.get(i).toString()));
                        }
                    }
                } else {
                    nameValuePairs.add(new BasicNameValuePair(keyfordata, aObj.getString(key)));
                }
            } catch (JSONException e) {
                return nameValuePairs;
            }
            index = index + 1;
        }
        return nameValuePairs;
    }

}
