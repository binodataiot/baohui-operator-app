package www.baohui.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import java.util.List;

import www.baohui.com.R;
import www.baohui.com.model.res.JsonGroupListValue;

public class GroupChangeAdapter extends BaseAdapter {
    private Context mContext;
    private List<JsonGroupListValue> value;

    public GroupChangeAdapter(Context context , List<JsonGroupListValue> value) {
        mContext = context;
        this.value = value ;
    }

    @Override
    public int getCount() {
        return (value == null || value.size() == 0 ) ? 0 : value.size();
    }

    @Override
    public Object getItem(int position) {
        return value.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void select(int position) {
        JsonGroupListValue data = this.value.get(position);
        if (!data.isSelected()) {
            data.setSelected(true);
            for (int i = 0; i < this.value.size(); i++) {
                if (i != position) {
                    this.value.get(i).setSelected(false);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        convertView = LayoutInflater.from(mContext).inflate(R.layout.group_change_list_item, parent, false);
        holder = new ViewHolder();
        holder.title = (TextView) convertView.findViewById(R.id.item_title);
        holder.titleLv2 = (TextView) convertView.findViewById(R.id.item_title_lv2);
        holder.radioButton = (RadioButton) convertView.findViewById(R.id.item_radioButton);
        holder.radioButton.setTag(position);
        convertView.setTag(holder);

        JsonGroupListValue data = value.get(position);
        holder.title.setText(data.getGroupName());
        holder.titleLv2.setText(data.getDeviceNo() + mContext.getString(R.string.system_device_unit));
        holder.radioButton.setChecked(data.isSelected());

        holder.radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton checkBox, boolean isChecked) {
                if(isChecked){
                    checkBox.setChecked(isChecked);
                    int pos = (int) checkBox.getTag();
                    select(pos);
                }
            }
        });
        return convertView;
    }

    public static class ViewHolder {
        public TextView title;
        public TextView titleLv2;
        public RadioButton radioButton;
    }

}
