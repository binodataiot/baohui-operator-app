package www.baohui.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import www.baohui.com.R;
import www.baohui.com.model.res.JsonDeviceListValue;

public class GroupListAdapter extends BaseAdapter {
    private Context mContext;
    private List<JsonDeviceListValue> value;

    public GroupListAdapter(Context context,List<JsonDeviceListValue> value) {
        mContext = context;
        this.value = value;
    }

    @Override
    public int getCount() {
        return (value == null || value.size() == 0 ) ? 0 : value.size();
    }

    @Override
    public Object getItem(int position) {
        return this.value.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        convertView = LayoutInflater.from(mContext).inflate(R.layout.group_list_item, parent, false);
        holder = new ViewHolder();
        holder.title = (TextView) convertView.findViewById(R.id.item_title);
        holder.titleLv2 = (TextView) convertView.findViewById(R.id.item_title_lv2);
        holder.replenished = (ImageView) convertView.findViewById(R.id.item_replenished);
        holder.fail = (ImageView) convertView.findViewById(R.id.item_fail);
        holder.lock = (ImageView) convertView.findViewById(R.id.item_lock);
        convertView.setTag(holder);

        JsonDeviceListValue data = this.value.get(position);
        holder.title.setText(data.getName());
        holder.titleLv2.setText(data.getDeviceTypeName());
        holder.replenished.setVisibility((data.getReplenished()) ? View.VISIBLE : View.GONE);
        holder.fail.setVisibility((data.getFail()) ? View.VISIBLE : View.GONE);
        holder.lock.setVisibility((data.getLock()) ? View.VISIBLE : View.GONE);

        return convertView;
    }

    public static class ViewHolder {
        public TextView title;
        public TextView titleLv2;
        public ImageView replenished;
        public ImageView fail;
        public ImageView lock;
    }

}
