package www.baohui.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import www.baohui.com.R;
import www.baohui.com.model.res.JsonGroupListValue;
import www.baohui.com.service.ImageViewClick;

public class DeviceListAdapter extends BaseAdapter {
    private Context mContext;
    private List<JsonGroupListValue> value;
    private View.OnClickListener deleteClickListener ;

    public DeviceListAdapter(Context context , List<JsonGroupListValue> value , View.OnClickListener deleteClickListener) {
        mContext = context;
        this.value = value ;
        this.deleteClickListener = deleteClickListener;
    }

    @Override
    public int getCount() {
        return (value == null || value.size() == 0 ) ? 0 : value.size();
    }

    @Override
    public Object getItem(int position) {
        return value.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        convertView = LayoutInflater.from(mContext).inflate(R.layout.device_list_item, parent, false);
        holder = new ViewHolder();
        holder.title = (TextView) convertView.findViewById(R.id.item_title);
        holder.titleLv2 = (TextView) convertView.findViewById(R.id.item_title_lv2);
        holder.delete = (ImageViewClick) convertView.findViewById(R.id.item_delete);
        holder.delete.setTag(position);
        convertView.setTag(holder);

        JsonGroupListValue data = value.get(position);
        holder.title.setText(data.getGroupName());
        holder.titleLv2.setText(data.getDeviceNo() + mContext.getString(R.string.system_device_unit));
        holder.delete.setVisibility((Integer.valueOf(data.getDeviceNo()) == 0) ? View.VISIBLE : View.GONE);

        if(deleteClickListener != null){
            holder.delete.setOnClickListener(deleteClickListener);
        }

        return convertView;
    }

    public static class ViewHolder {
        public TextView title;
        public TextView titleLv2;
        public ImageViewClick delete;
    }

}
