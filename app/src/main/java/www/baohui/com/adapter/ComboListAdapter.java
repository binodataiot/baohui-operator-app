package www.baohui.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import www.baohui.com.R;
import www.baohui.com.model.res.JsonComboListValue;

public class ComboListAdapter extends BaseAdapter {
    private Context mContext;
    private List<JsonComboListValue> value;

    public ComboListAdapter(Context context, List<JsonComboListValue> value) {
        mContext = context;
        this.value = value;
    }

    @Override
    public int getCount() {
        return (value == null || value.size() == 0 ) ? 0 : value.size();
    }

    @Override
    public Object getItem(int position) {
        return this.value.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        convertView = LayoutInflater.from(mContext).inflate(R.layout.combo_list_delete_item, parent, false);
        holder = new ViewHolder();
        holder.title = (TextView) convertView.findViewById(R.id.item_title);
        holder.titleLv2 = (TextView) convertView.findViewById(R.id.item_title_lv2);
        convertView.setTag(holder);

        JsonComboListValue data = this.value.get(position);


        holder.title.setText(data.getName());
        holder.titleLv2.setText(data.getPrice()+"元" + data.getRounds() +"次");

        return convertView;
    }

    public static class ViewHolder {
        public TextView title;
        public TextView titleLv2;
    }

}
