package www.baohui.com.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.List;

import www.baohui.com.R;
import www.baohui.com.model.res.JsonComboListValue;

public class ComboInListAdapter extends BaseAdapter {
    private Context mContext;
    private List<JsonComboListValue> value;

    public ComboInListAdapter(Context context, List<JsonComboListValue> value) {
        mContext = context;
        this.value = value;
    }

    @Override
    public int getCount() {
        return (value == null || value.size() == 0 ) ? 0 : value.size();
    }

    @Override
    public Object getItem(int position) {
        return this.value.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        convertView = LayoutInflater.from(mContext).inflate(R.layout.combo_in_list_item, parent, false);
        holder = new ViewHolder();
        holder.title = (TextView) convertView.findViewById(R.id.item_title);
        holder.titleLv2 = (TextView) convertView.findViewById(R.id.item_title_lv2);
        holder.checkBox = (CheckBox) convertView.findViewById(R.id.item_checkBox);
        holder.checkBox.setTag(position);
        convertView.setTag(holder);

        JsonComboListValue data = this.value.get(position);

        holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton checkBox , boolean isChecked) {
                checkBox.setChecked(isChecked);
                int pos = (int) checkBox.getTag();
                value.get(pos).setCheckBox(isChecked);
            }
        });

        holder.title.setText(data.getName());
        holder.titleLv2.setText(data.getPrice()+"元" + data.getRounds() +"次");

        return convertView;
    }

    public static class ViewHolder {
        public TextView title;
        public TextView titleLv2;
        public CheckBox checkBox;
    }

}
