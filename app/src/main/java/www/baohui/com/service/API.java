package www.baohui.com.service;

import android.util.Log;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.HashMap;

import one.customutil.GsonAsyncTask;
import www.baohui.com.model.req.JsonMailSendEmailDeviceIdObj;
import www.baohui.com.model.req.JsonOperatorDeviceStartObj;
import www.baohui.com.model.req.JsonOperatorWechatUpdateObj;
import www.baohui.com.model.res.JsonRegisterValue;
import www.baohui.com.model.req.JsonComboCreateObj;
import www.baohui.com.model.req.JsonComboDeleteObj;
import www.baohui.com.model.req.JsonDeviceActiveObj;
import www.baohui.com.model.req.JsonDeviceCheckQRCodeObj;
import www.baohui.com.model.req.JsonDeviceComboApplyObj;
import www.baohui.com.model.req.JsonDeviceComboDeleteObj;
import www.baohui.com.model.req.JsonDeviceComboListObj;
import www.baohui.com.model.req.JsonDeviceDeactiveObj;
import www.baohui.com.model.req.JsonDeviceDetailedObj;
import www.baohui.com.model.req.JsonDeviceGroupUpdateObj;
import www.baohui.com.model.req.JsonDeviceListObj;
import www.baohui.com.model.req.JsonDeviceUpdateObj;
import www.baohui.com.model.req.JsonGroupCreateObj;
import www.baohui.com.model.req.JsonGroupDeleteObj;
import www.baohui.com.model.req.JsonGroupUpDateObj;
import www.baohui.com.model.req.JsonLoginObj;
import www.baohui.com.model.req.JsonOperatorAuthConfirmObj;
import www.baohui.com.model.req.JsonOperatorPasswordUpdateObj;
import www.baohui.com.model.req.JsonRegisterObj;
import www.baohui.com.model.res.JsonComboListValue;
import www.baohui.com.model.res.JsonDeviceCheckQRCodeValue;
import www.baohui.com.model.res.JsonDeviceDetailedValue;
import www.baohui.com.model.res.JsonDeviceListValue;
import www.baohui.com.model.res.JsonGroupListValue;
import www.baohui.com.model.res.JsonListObj;
import www.baohui.com.model.res.JsonOperatorAuthConfirmValue;
import www.baohui.com.model.res.JsonResponseObj;

/**
 * util
 * API工具
 */
public class API {
    public static final String packageName = "WWW.BAOHUI.COM";
    private static final String TAG = "API";
    private static HashMap<String, String> header;
    private static String token;
    private static String Host = "";
    private static final String API = "/api";
    private static final String OPERATOR = "/Operator";
    private static final String GROUP = "/Group";
    private static final String DEVICE = "/Device";
    private static final String COMBO = "/Combo";
    private static final int RCN0 = 0;

    public static void setHost(String host) {
        Host = host;
    }

    public static void setToken(String token) {
        www.baohui.com.service.API.token = Util.base64Encode(token);
    }

    public static String getToken() {
        return Util.base64Decode(token);
    }

    public static HashMap<String, String> getHeader() {
        header = new HashMap<String, String>();
        header.put("token", getToken());
        System.out.println("getToken() = " + getToken());
        return header;
    }

    public static boolean resultCodeCheck(int resultCode) {
        if (resultCode == RCN0) {
            return true;
        }
        return false;
    }

    public static void register(GsonAsyncTask.onPostExecute onPostExecute, JsonRegisterObj obj) {
        String url = Host + API + OPERATOR + "/Register";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType();
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(1);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void login(GsonAsyncTask.onPostExecute onPostExecute, JsonLoginObj obj) {
        String url = Host + API + OPERATOR + "/Login";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType();
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(2);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void groupCreate(GsonAsyncTask.onPostExecute onPostExecute , JsonGroupCreateObj obj) {
        String url = Host + API + GROUP + "/Create";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType();
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setMissionCode(3);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void groupUpDate(GsonAsyncTask.onPostExecute onPostExecute , JsonGroupUpDateObj obj) {
        String url = Host + API + GROUP + "/UpDate";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(4);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void groupList(GsonAsyncTask.onPostExecute onPostExecute) {
        String url = Host + API + GROUP + "/List";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<JsonListObj<JsonGroupListValue>>>() {}.getType();
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(5);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_GETRESPONSE);
        task.setHeader(getHeader());
        task.run(url);
    }

    public static void deviceList(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceListObj obj) {
        String url = Host + API + DEVICE + "/List";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<JsonListObj<JsonDeviceListValue>>>() {}.getType() ;
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(7);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void deviceDetailed(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceDetailedObj obj) {
        String url = Host + API + DEVICE + "/Detail";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<JsonDeviceDetailedValue>>() {}.getType() ;
        GsonAsyncTask task = new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(8);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void deviceCheckQRCode(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceCheckQRCodeObj obj) {
        String url = Host + API + DEVICE + "/CheckQRCode";
        Log.e(TAG, "url = " + url);
        Type objType =  new TypeToken<JsonResponseObj<JsonDeviceCheckQRCodeValue>>() {}.getType() ;
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setMissionCode(9);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void deviceActive(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceActiveObj obj) {
        String url = Host + API + DEVICE + "/Active";
        Log.e(TAG, "url = " + url);
        Type objType =  new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(10);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void deviceDeactive(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceDeactiveObj obj) {
        String url = Host + API + DEVICE + "/Deactive";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType();
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(11);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void comboList(GsonAsyncTask.onPostExecute onPostExecute) {
        String url = Host + API + COMBO + "/List";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<JsonListObj<JsonComboListValue>>>() {}.getType() ;
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(12);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_GETRESPONSE);
        task.setHeader(getHeader());
        task.run(url);
    }

    public static void comboCreate(GsonAsyncTask.onPostExecute onPostExecute , JsonComboCreateObj obj) {
        String url = Host + API + COMBO + "/Create";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(13);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void comboDelete(GsonAsyncTask.onPostExecute onPostExecute , JsonComboDeleteObj obj) {
        String url = Host + API + COMBO + "/Delete";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(14);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void deviceComboList(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceComboListObj obj) {
        String url = Host + API + DEVICE + COMBO + "/List";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<JsonListObj<JsonComboListValue>>>() {}.getType() ;
        GsonAsyncTask task = new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(15);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void deviceComboApply(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceComboApplyObj obj) {
        String url = Host + API + DEVICE + COMBO + "/Apply";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task = new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(16);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void deviceComboDelete(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceComboDeleteObj obj) {
        String url = Host + API + DEVICE + COMBO + "/Delete";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task = new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(17);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void operatorAuthConfirm(GsonAsyncTask.onPostExecute onPostExecute, JsonOperatorAuthConfirmObj obj) {
        String url = Host + API + OPERATOR + "/Auth/Confirm";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<JsonOperatorAuthConfirmValue>>() {}.getType();
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(18);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setParams(obj.getParams());
        task.setHeader(getHeader());
        task.run(url);
    }

    public static void mailSendEmailDeviceId(GsonAsyncTask.onPostExecute onPostExecute, String deviceId, JsonMailSendEmailDeviceIdObj obj) {
        String url = Host + API + "/Mail/Send/" + deviceId;
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task = new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(21);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void deviceUpdate(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceUpdateObj obj) {
        String url = Host + API + DEVICE + "/Update";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task = new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(22);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void operatorPasswordUpdate(GsonAsyncTask.onPostExecute onPostExecute , JsonOperatorPasswordUpdateObj obj) {
        String url = Host + API + OPERATOR + "/Password/Update";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType();
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setParams(obj.getParams());
        task.setMissionCode(23);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.run(url);
    }

    public static void groupDelete(GsonAsyncTask.onPostExecute onPostExecute , JsonGroupDeleteObj obj) {
        String url = Host + API + GROUP + "/Delete";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(24);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void deviceGroupUpdate(GsonAsyncTask.onPostExecute onPostExecute, JsonDeviceGroupUpdateObj obj) {
        String url = Host + API + DEVICE + GROUP +"/Update";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType() ;
        GsonAsyncTask task = new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(25);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void operatorDetail(GsonAsyncTask.onPostExecute onPostExecute) {
        String url = Host + API + OPERATOR + "/Detail";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<JsonRegisterValue>>() {}.getType();
        GsonAsyncTask task= new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(26);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_GETRESPONSE);
        task.setHeader(getHeader());
        task.run(url);
    }

    public static String deviceProfit (){
        return Host + API + DEVICE + "/Profit/";
    }

    public static String policiesTerms (){
        return Host + "/Policies/Terms";
    }

    public static void operatorWechatUpdate(GsonAsyncTask.onPostExecute onPostExecute , JsonOperatorWechatUpdateObj obj) {
        String url = Host + API + OPERATOR + "/Wechat/Update";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType();
        GsonAsyncTask task = new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(27);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }

    public static void operatorDeviceStart(GsonAsyncTask.onPostExecute onPostExecute , JsonOperatorDeviceStartObj obj) {
        String url = Host + API + OPERATOR + DEVICE + "/Start";
        Log.e(TAG, "url = " + url);
        Type objType = new TypeToken<JsonResponseObj<String>>() {}.getType();
        GsonAsyncTask task = new GsonAsyncTask(onPostExecute, objType);
        task.setMissionCode(28);
        task.setHttpMethod(GsonAsyncTask.HTTP_TYPE_POSTRESPONSE);
        task.setHeader(getHeader());
        task.setParams(obj.getParams());
        task.run(url);
    }


}