package www.baohui.com.service;

import android.content.Context;
import android.support.v4.app.Fragment;

import one.customutil.GsonAsyncTask;
import www.baohui.com.R;
import www.baohui.com.activity.MainActivity;
import www.baohui.com.model.res.JsonListObj;
import www.baohui.com.model.res.JsonResponseObj;

/**
 * util
 * BaseFragment
 */
public class BaseFragment extends Fragment implements GsonAsyncTask.onPostExecute {

    /**
     * dialog msg
     */
    public void showDialog(Context context,String mag){
        final MAlertDialog mAlertDialog = new MAlertDialog(context);
        mAlertDialog.setMsg(mag);
        mAlertDialog.show();
    }

    /**
     *檢查傳輸連線錯誤
     */
    public boolean ckeckError(Object result , MainActivity mContext){
        if (result instanceof JsonResponseObj) {
            JsonResponseObj data = (JsonResponseObj) result;

            if (!API.resultCodeCheck(data.getResultCode())) {
                showDialog(mContext, data.getResultMessage());
                return false;
            }

            if (data.getValue() instanceof JsonListObj) {
                JsonListObj jsonListObj = (JsonListObj) data.getValue();
                if (!API.resultCodeCheck(Integer.valueOf(jsonListObj.getErrorCode()))) {
                    showDialog(mContext, jsonListObj.getErrorMessage());
                    return false;
                }
            }

        } else {
            showDialog(mContext,getResources().getString(R.string.error_network_s));
            return false ;
        }

        return true;
    }

    @Override
    public void onPostExecute(Object o , int i) {

    }
}
