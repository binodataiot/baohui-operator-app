package www.baohui.com.service;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.app.AlertDialog;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import www.baohui.com.R;

/**
 * util
 * 所有的Dialog
 */
public class MAlertDialog extends AlertDialog.Builder {
    private Context context;
    private View dialogView;
    private Dialog dialog;

    private TextView msg;
    private EditText edit;
    private EditText edit2;
    private EditText edit3;
    private EditText edit4;
    private EditText edit5;
    private Button cancel;
    private Button ok;

    public MAlertDialog(Context context) {
        super(context);
        init(context);
    }

    public MAlertDialog(Context context, int arg1) {
        super(context, arg1);
        init(context);
    }

    private void init(Context context) {
        this.context = context;
        dialogView = LayoutInflater.from(context).inflate(context.getResources().getLayout(R.layout.m_alertdialog_view), null);
        setView(dialogView);
        findView();
        onListener();
    }

    private void findView() {
        msg = (TextView) dialogView.findViewById(R.id.ad_msg);
        edit = (EditText) dialogView.findViewById(R.id.ad_edit);
        edit2 = (EditText) dialogView.findViewById(R.id.ad_edit2);
        edit3 = (EditText) dialogView.findViewById(R.id.ad_edit3);
        edit4 = (EditText) dialogView.findViewById(R.id.ad_edit4);
        edit5 = (EditText) dialogView.findViewById(R.id.ad_edit5);
        cancel = (Button) dialogView.findViewById(R.id.ad_cancel);
        ok = (Button) dialogView.findViewById(R.id.ad_ok);
        ok.setVisibility(View.VISIBLE);
    }

    private void onListener() {
        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
        ok.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View arg0) {
                dismiss();
            }
        });
    }

    public void setMsg(String msg) {
        this.msg.setText(msg);
    }

    public void setEditType(int type){
        this.edit.setInputType(type);
    }

    public void setEdit2Type(int type){
        this.edit2.setInputType(type);
    }

    public void setEdit3Type(int type){
        this.edit3.setInputType(type);
    }

    public void setEdit4Type(int type){
        this.edit4.setInputType(type);
    }

    public void setEdit5Type(int type){
        this.edit5.setInputType(type);
    }

    public void setEditKeyListener(DigitsKeyListener instance){
        this.edit.setKeyListener(instance);
    }

    public void setEdit2KeyListener(DigitsKeyListener instance){
        this.edit2.setKeyListener(instance);
    }

    public void setEdit3KeyListener(DigitsKeyListener instance){
        this.edit3.setKeyListener(instance);
    }

    public void setEdit4KeyListener(DigitsKeyListener instance){
        this.edit4.setKeyListener(instance);
    }

    public void setEdit5KeyListener(DigitsKeyListener instance){
        this.edit5.setKeyListener(instance);
    }

    public void setEditHint(String msg) {
        if (msg != null) {
            this.edit.setHint(msg);
            this.edit.setVisibility(View.VISIBLE);
        } else {
            this.edit.setVisibility(View.INVISIBLE);
        }
    }

    public void setEdit2Hint(String msg) {
        if (msg != null) {
            this.edit2.setHint(msg);
            this.edit2.setVisibility(View.VISIBLE);
        } else {
            this.edit2.setVisibility(View.GONE);
        }
    }

    public void setEdit3Hint(String msg) {
        if (msg != null) {
            this.edit3.setHint(msg);
            this.edit3.setVisibility(View.VISIBLE);
        } else {
            this.edit3.setVisibility(View.GONE);
        }
    }

    public void setEdit4Hint(String msg) {
        if (msg != null) {
            this.edit4.setHint(msg);
            this.edit4.setVisibility(View.VISIBLE);
        } else {
            this.edit4.setVisibility(View.GONE);
        }
    }

    public void setEdit5Hint(String msg) {
        if (msg != null) {
            this.edit5.setHint(msg);
            this.edit5.setVisibility(View.VISIBLE);
        } else {
            this.edit5.setVisibility(View.GONE);
        }
    }

    public void setEditText(String msg) {
        if (msg != null) {
            this.edit.setText(msg);
            this.edit.setVisibility(View.VISIBLE);
        } else {
            this.edit.setVisibility(View.INVISIBLE);
        }
    }

    public void setEdit2Text(String msg) {
        if (msg != null) {
            this.edit2.setText(msg);
            this.edit2.setVisibility(View.VISIBLE);
        } else {
            this.edit2.setVisibility(View.INVISIBLE);
        }
    }

    public void setEdit3Text(String msg) {
        if (msg != null) {
            this.edit3.setText(msg);
            this.edit3.setVisibility(View.VISIBLE);
        } else {
            this.edit3.setVisibility(View.INVISIBLE);
        }
    }

    public void setEdit4Text(String msg) {
        if (msg != null) {
            this.edit4.setText(msg);
            this.edit4.setVisibility(View.VISIBLE);
        } else {
            this.edit4.setVisibility(View.INVISIBLE);
        }
    }

    public void setEdit5Text(String msg) {
        if (msg != null) {
            this.edit5.setText(msg);
            this.edit5.setVisibility(View.VISIBLE);
        } else {
            this.edit5.setVisibility(View.INVISIBLE);
        }
    }

    public String getEditText() {
        return this.edit.getText().toString();
    }

    public String getEdit2Text() {
        return this.edit2.getText().toString();
    }

    public String getEdit3Text() {
        return this.edit3.getText().toString();
    }

    public String getEdit4Text() {
        return this.edit4.getText().toString();
    }

    public String getEdit5Text() {
        return this.edit5.getText().toString();
    }

    public void setOkOnListener(View.OnClickListener onClickListener) {
        if (onClickListener != null) {
            ok.setOnClickListener(onClickListener);
            ok.setVisibility(View.VISIBLE);
        } else {
            ok.setVisibility(View.INVISIBLE);
        }
    }

    public void showCancel(boolean b) {
        if (b) {
            cancel.setVisibility(View.VISIBLE);
        } else {
            cancel.setVisibility(View.INVISIBLE);
        }
    }

    public void dismiss() {
        if (dialog != null) {
            dialog.dismiss();
        }
    }

    @Override
    public AlertDialog show() {
        dialog = MAlertDialog.this.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.show();
        return null;
    }

}
