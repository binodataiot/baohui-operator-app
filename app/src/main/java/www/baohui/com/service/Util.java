package www.baohui.com.service;

import android.content.Context;
import android.util.Base64;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import www.baohui.com.R;
import www.baohui.com.model.res.JsonComboListValue;

/**
 * util
 * 小工具
 */
public class Util {
    public static String base64Encode(String s){
        return Base64.encodeToString(s.getBytes(), Base64.DEFAULT);
    }

    public static String base64Decode(String s){
        return new String(Base64.decode(s, Base64.DEFAULT));
    }

    /**
     * 日期格式轉換
     */
    public static String dateChange(Context context , String s){
        try {
            String sdf = "yyyy" + context.getResources().getString(R.string.system_yy) + "MM" +context.getResources().getString(R.string.system_mm)+"dd"+context.getResources().getString(R.string.system_dd);
//            return new SimpleDateFormat(sdf).format(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.S").parse(s));
            return new SimpleDateFormat(sdf).format(new SimpleDateFormat("yyyyMMddHHmmss").parse(s));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return s;
    }

    /**
     * 比對List Obj重複
     */
    public static List<JsonComboListValue> repeatJsonComboListValue(List<JsonComboListValue> list1, List<JsonComboListValue> list2) {
        List<JsonComboListValue> result = new ArrayList<JsonComboListValue>();
        List<JsonComboListValue> maxList = list1;
        List<JsonComboListValue> minList = list2;
        if (list2.size() > list1.size()) {
            maxList = list2;
            minList = list1;
        }
        HashMap<String, JsonComboListValue> map = new HashMap<String, JsonComboListValue>(maxList.size());
        for (JsonComboListValue data : maxList) {
            data.setRepeatTag(true);
            map.put(data.getComboId().toString(), data);
        }
        for (JsonComboListValue data : minList) {
            if (map.get(data.getComboId().toString()) != null) {
                data.setRepeatTag(false);
                map.put(data.getComboId().toString(), data);
                continue;
            }
            result.add(data);
        }
        for (Map.Entry<String, JsonComboListValue> entry : map.entrySet()) {
            if (entry.getValue().isRepeatTag() == true) {
                result.add(entry.getValue());
            }
        }
        return result;
    }

    /**
     * 檢查Email格式
     */
    public static final Pattern EMAIL_PATTERN = Pattern.compile("^\\w+\\.*\\w+@(\\w+\\.){1,5}[a-zA-Z]{2,3}$");

    /**
     * 檢查Email格式
     */
    public static boolean isValidEmail(String email) {
        boolean result = false;
        if (EMAIL_PATTERN.matcher(email).matches()) {
            result = true;
        }
        return result;
    }

    /**
     *檢查是否為數字及小數點
     */
    public static boolean isDECIMAL(String str) {
        Pattern pattern = Pattern.compile("[0-9].*");
        Matcher isNum = pattern.matcher(str);
        if( !isNum.matches() )
        {
            return false;
        }
        return true;
    }

}
