package www.baohui.com.service;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import one.customutil.GsonAsyncTask;
import www.baohui.com.R;
import www.baohui.com.model.res.JsonListObj;
import www.baohui.com.model.res.JsonResponseObj;

/**
 * util
 * BaseAppCompatActivity
 */
public class BaseAppCompatActivity extends AppCompatActivity implements GsonAsyncTask.onPostExecute {
    private ProgressDialog mProgressDialog;
    protected OnBackPressedListener onBackPressedListener;

    public interface OnBackPressedListener {
        void onBackPressed();
    }

    public void setOnBackPressedListener(OnBackPressedListener onBackPressedListener) {
        this.onBackPressedListener = onBackPressedListener;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setMessage(getResources().getString(R.string.system_load));
        mProgressDialog.setCanceledOnTouchOutside(false);
        mProgressDialog.setCancelable(false);
    }

    @Override
    public void onBackPressed() {
        if (onBackPressedListener != null){
            onBackPressedListener.onBackPressed();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        if (onBackPressedListener != null){
            onBackPressedListener = null;
        }
        super.onDestroy();
    }
    /**
     * 開啟畫面轉圈
     */
    public void showProgress(){
        mProgressDialog.show();
    }

    /**
     * 關閉畫面轉圈
     */
    public void dismissProgress(){
        mProgressDialog.dismiss();
    }

    public void showDialog(String mag){
        final MAlertDialog mAlertDialog = new MAlertDialog(this);
        mAlertDialog.setMsg(mag);
        mAlertDialog.show();
    }

    /**
     *檢查傳輸連線錯誤
     */
    public boolean ckeckError(Object result){
        if (result instanceof JsonResponseObj) {
            JsonResponseObj data = (JsonResponseObj) result;

            if (!API.resultCodeCheck(data.getResultCode())) {
                showDialog(data.getResultMessage());
                return false;
            }

            if (data.getValue() instanceof JsonListObj) {
                JsonListObj jsonListObj = (JsonListObj) data.getValue();
                if (!API.resultCodeCheck(Integer.valueOf(jsonListObj.getErrorCode()))) {
                    showDialog(jsonListObj.getErrorMessage());
                    return false;
                }
            }

        } else {
            showDialog(getResources().getString(R.string.error_network_s));
            return false ;
        }

        return true;
    }

    @Override
    public void onPostExecute(Object o, int i) {

    }
}
