package www.baohui.com.device;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.google.zxing.Result;

import java.util.List;

import me.dm7.barcodescanner.zxing.ZXingScannerView;
import www.baohui.com.activity.MainActivity;
import www.baohui.com.R;
import www.baohui.com.adapter.GroupListAdapter;
import www.baohui.com.model.req.JsonDeviceActiveObj;
import www.baohui.com.model.req.JsonDeviceCheckQRCodeObj;
import www.baohui.com.model.req.JsonDeviceListObj;
import www.baohui.com.model.res.JsonDeviceCheckQRCodeValue;
import www.baohui.com.model.res.JsonDeviceListValue;
import www.baohui.com.model.res.JsonGroupListValue;
import www.baohui.com.model.res.JsonListObj;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseAppCompatActivity;
import www.baohui.com.service.BaseFragment;
import www.baohui.com.service.MAlertDialog;

/**
 * page
 * 群組-機台列表
 */
@SuppressLint("ValidFragment")
public class GroupListView extends BaseFragment implements ZXingScannerView.ResultHandler , BaseAppCompatActivity.OnBackPressedListener {
    private MainActivity mContext;
    private RelativeLayout scannerRelativeView;
    private ZXingScannerView scannerView;
    private View view;
    private GroupListAdapter adapter;
    private String title ;

    private ListView listView;
    private ImageView imageView;

    private JsonGroupListValue posValue;
    private List<JsonDeviceListValue> value;

    private String sn = "";

    public GroupListView(MainActivity context , JsonGroupListValue value) {
        mContext = context ;
        this.posValue = value ;
        this.title = this.posValue.getGroupName();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(title);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (scannerView != null) {
            scannerView.stopCamera();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.group_list_view, container, false);
        findView();
        initData();
        onListener();
        return this.view;
    }

    private void findView() {
        listView = (ListView) this.view.findViewById(R.id.listView);
        imageView = (ImageView) this.view.findViewById(R.id.imageView);
        scannerRelativeView = (RelativeLayout) this.view.findViewById(R.id.relativeLayout);
    }

    private void initData() {
        callAPI();
        scannerView = new ZXingScannerView(mContext);
    }

    /**
     * API
     * 機台列表
     */
    private void callAPI() {
        mContext.showProgress();
        JsonDeviceListObj jsonDeviceListObj = new JsonDeviceListObj();
        jsonDeviceListObj.setGroupId(String.valueOf(posValue.getGroupId()));
        API.deviceList(this,jsonDeviceListObj);
    }

    /**
     *API
     * 新機台建立
     */
    private void callDeviceActiveAPI(JsonDeviceActiveObj obj) {
        mContext.showProgress();
        API.deviceActive(this,obj);
    }

    /**
     *API
     * 檢查機台狀況
     */
    private void callDeviceCheckQRCodeAPI(String sn) {
        this.sn = sn ;
        mContext.showProgress();
        JsonDeviceCheckQRCodeObj obj = new JsonDeviceCheckQRCodeObj();
        obj.setSN(sn);
        API.deviceCheckQRCode(this,obj);
    }

    private void onListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JsonDeviceListValue posValue = (JsonDeviceListValue) adapter.getItem(position);
                posValue.setGroupId(GroupListView.this.posValue.getGroupId());
                mContext.changeFragment(new DeviceDetailedView(mContext, posValue));
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_device_add_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_add:
                startScanning(GroupListView.this);
                return true;
            default:
                break;
        }
        return false;
    }

    /**
     * 關閉QR掃描
     */
    public void closeScanner(){
        if (scannerView != null) {
            mContext.setOnBackPressedListener(null);
            scannerView.stopCamera();
            scannerRelativeView.setVisibility(View.GONE);
        }
    }

    /**
     *開啟QR掃描
     */
    public void startScanning(Fragment fragment) {
        if (ContextCompat.checkSelfPermission(mContext, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            if (!fragment.shouldShowRequestPermissionRationale(Manifest.permission.CAMERA)) {
                fragment.requestPermissions(new String[]{Manifest.permission.CAMERA}, 100);
            }
        } else {
            if(scannerView!=null){
                scannerView.startCamera();
                scannerView.setResultHandler((ZXingScannerView.ResultHandler) fragment);
                scannerRelativeView.removeAllViews();
                scannerRelativeView.addView(scannerView, new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                scannerRelativeView.setVisibility(View.VISIBLE);
                mContext.setOnBackPressedListener(this);
            }
        }
    }

    /**
     *QRCode資訊回傳
     */
    @Override
    public void handleResult(Result result) {
        String qrcode = result.getText();
        closeScanner();
        callDeviceCheckQRCodeAPI(qrcode);
    }

    private void callBindDialog(final String sn) {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setEditHint(getResources().getString(R.string.gdc_page_setting_device_name_s));
        mAlertDialog.setMsg(getResources().getString(R.string.s_n_s) + "：\n" + sn);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = mAlertDialog.getEditText();
                if (value != null && value.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return ;
                } else {
                    JsonDeviceActiveObj obj = new JsonDeviceActiveObj();
                    obj.setSN(sn);
                    obj.setName(value);
                    obj.setGroupId(posValue.getGroupId().toString());
                    callDeviceActiveAPI(obj);
                }
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    @Override
    public void onBackPressed() {
        if(scannerRelativeView.getVisibility() == View.VISIBLE){
            closeScanner();
        }
    }

    /**
     * QRCode檢查權限回傳
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 100: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startScanning(GroupListView.this);
                }
                return;
            }
        }
    }

    private void setListView(List<JsonDeviceListValue> value) {
        this.value = value;
        adapter = new GroupListAdapter(mContext,this.value);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        imageView.setVisibility((adapter.getCount() != 0) ? View.GONE : View.VISIBLE);
    }

    /**
     *API回傳
     */
    @Override
    public void onPostExecute(Object result , int missionCode) {
        mContext.dismissProgress();
        if(!ckeckError(result,mContext)){
            // 檢驗基礎傳輸錯誤
            return ;
        }
        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 9){
            // deviceCheckQRCode
            JsonDeviceCheckQRCodeValue value = (JsonDeviceCheckQRCodeValue) data.getValue();
            if(Boolean.valueOf(value.getIsOperatorBinded())){
                showDialog(mContext,getString(R.string.gdc_page_bind_check_error_s));
            } else {
                callBindDialog(this.sn);
            }
        }

        if(missionCode == 7){
            // deviceList
            JsonListObj jsonListObj = (JsonListObj) data.getValue();
            setListView(jsonListObj.getValue());
        }

        if(missionCode == 10){
            // deviceActive
            showDialog(mContext,getString(R.string.gdc_page_bind_ok_s));
            callAPI();
        }
    }

}
