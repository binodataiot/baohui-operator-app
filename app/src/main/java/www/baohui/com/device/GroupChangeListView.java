package www.baohui.com.device;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.List;

import www.baohui.com.activity.MainActivity;
import www.baohui.com.R;
import www.baohui.com.adapter.GroupChangeAdapter;
import www.baohui.com.model.req.JsonDeviceGroupUpdateObj;
import www.baohui.com.model.res.JsonDeviceListValue;
import www.baohui.com.model.res.JsonGroupListValue;
import www.baohui.com.model.res.JsonListObj;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseFragment;

/**
 * page
 * 群組變更頁面
 */
@SuppressLint("ValidFragment")
public class GroupChangeListView extends BaseFragment {
    private MainActivity mContext;
    private View view;
    private GroupChangeAdapter adapter;

    private ListView listView;
    private ImageView imageView;
    private Button ok;

    private JsonDeviceListValue posValue;
    private List<JsonGroupListValue> value;


    public GroupChangeListView(MainActivity context, JsonDeviceListValue value) {
        mContext = context ;
        this.posValue = value ;
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getResources().getString(R.string.gdc_page_ch_group_s));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.group_change_list_view, container, false);
        findView();
        initData();
        onListener();
        return this.view;
    }

    private void findView() {
        listView = (ListView) this.view.findViewById(R.id.listView);
        imageView = (ImageView) this.view.findViewById(R.id.imageView);
        ok = (Button) this.view.findViewById(R.id.ok);
    }

    private void initData() {
        callAPI();
    }

    /**
     * API
     * 群組列表
     */
    private void callAPI() {
        mContext.showProgress();
        API.groupList(this);
    }

    private void callDeviceGroupUpdate(JsonGroupListValue data) {
        mContext.showProgress();
        JsonDeviceGroupUpdateObj obj = new JsonDeviceGroupUpdateObj();
        obj.setDeviceId(posValue.getDeviceId());
        obj.setGroupId(data.getGroupId().toString());
        API.deviceGroupUpdate(this,obj);
    }

    private void onListener() {
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < value.size(); i++) {
                    JsonGroupListValue data = value.get(i);
                    if (data.isSelected()) {
                        callDeviceGroupUpdate(data);
                        return ;
                    }
                }
                getFragmentManager().popBackStack();
            }
        });
    }

    private void setListView(List<JsonGroupListValue> value) {
        this.value = value;
        adapter = new GroupChangeAdapter(mContext, this.value);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        imageView.setVisibility((adapter.getCount() != 0) ? View.GONE : View.VISIBLE);
    }

    /**
     *API回傳
     */
    public void onPostExecute(Object result , int missionCode) {
        mContext.dismissProgress();
        if(!ckeckError(result,mContext)){
            // 檢驗基礎傳輸錯誤
            return ;
        }

        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 25){
            // deviceGroupUpdate
            showDialog(mContext,getString(R.string.success_s));
            getFragmentManager().popBackStack();
            // 若需要回主頁面，多關閉兩次即可
//                getFragmentManager().popBackStack();
//                getFragmentManager().popBackStack();
        }

        if(missionCode == 5){
            // groupList
            JsonListObj jsonListObj = (JsonListObj) data.getValue();
            setListView(jsonListObj.getValue());
        }

    }

}
