package www.baohui.com.device;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import www.baohui.com.R;
import www.baohui.com.activity.MainActivity;
import www.baohui.com.combo.ComboManageListView;
import www.baohui.com.model.req.JsonDeviceDeactiveObj;
import www.baohui.com.model.req.JsonDeviceDetailedObj;
import www.baohui.com.model.req.JsonDeviceUpdateObj;
import www.baohui.com.model.req.JsonMailSendEmailDeviceIdObj;
import www.baohui.com.model.req.JsonOperatorAuthConfirmObj;
import www.baohui.com.model.req.JsonOperatorDeviceStartObj;
import www.baohui.com.model.res.JsonDeviceDetailedValue;
import www.baohui.com.model.res.JsonDeviceListValue;
import www.baohui.com.model.res.JsonOperatorAuthConfirmValue;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseFragment;
import www.baohui.com.service.MAlertDialog;
import www.baohui.com.service.Util;

/**
 * page
 * 機台詳細資料
 */
@SuppressLint("ValidFragment")
public class DeviceDetailedView extends BaseFragment {
    private MainActivity mContext;
    private View view;
    private String title ;

    private ImageView sales;
    private ImageView comboManage;
    private ImageView groupChange;
    private Button bindCancel;
    private Button deviceChName;
    private Button sendEmail;
    private Button bootUp;
    private TextView sn;
    private TextView deviceName;
    private TextView groupName;
    private TextView bindedDateTime;

    private JsonDeviceListValue posValue;
    private JsonDeviceDetailedValue value;

    public DeviceDetailedView(MainActivity context , JsonDeviceListValue value ) {
        mContext = context;
        this.posValue = value ;
        this.title = this.posValue.getName();
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(title);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.device_detailed_view, container, false);
        findView();
        initData();
        onListener();
        return this.view;
    }

    private void findView() {
        sales = (ImageView) this.view.findViewById(R.id.s_sales);
        comboManage = (ImageView) this.view.findViewById(R.id.s_combo_manage);
        groupChange = (ImageView) this.view.findViewById(R.id.s_group_change);
        bindCancel = (Button) this.view.findViewById(R.id.b_bind_cancel);
        deviceChName = (Button) this.view.findViewById(R.id.b_ch_device_name);
        sendEmail = (Button) this.view.findViewById(R.id.b_send_email);
        bootUp = (Button) this.view.findViewById(R.id.b_send_boot_up);
        sn = (TextView) this.view.findViewById(R.id.tv_sn);
        deviceName = (TextView) this.view.findViewById(R.id.tv_device_name);
        groupName = (TextView) this.view.findViewById(R.id.tv_group_name);
        bindedDateTime = (TextView) this.view.findViewById(R.id.tv_binded_date_time);
    }

    private void initData() {
        callAPI();
    }

    /**
     * API
     * 取得機台詳細資料
     */
    private void callAPI() {
        mContext.showProgress();
        JsonDeviceDetailedObj obj = new JsonDeviceDetailedObj();
        obj.setDeviceId(String.valueOf(posValue.getDeviceId()));
        API.deviceDetailed(this,obj);
    }

    /**
     *API
     * 密碼確認
     */
    private void callOperatorAuthConfirm(String value) {
        mContext.showProgress();
        JsonOperatorAuthConfirmObj obj = new JsonOperatorAuthConfirmObj();
        obj.setPassword(value);
        API.operatorAuthConfirm(this,obj);
    }

    /**
     * API
     * 解除機台綁定
     */
    private void callDeviceDeactiveAPI() {
        mContext.showProgress();
        JsonDeviceDeactiveObj obj = new JsonDeviceDeactiveObj();
        obj.setDeviceId(posValue.getDeviceId().toString());
        API.deviceDeactive(this,obj);
    }

    /**
     *API
     * 更改機台名稱
     */
    private void callDeviceUpdateAPI(String name) {
        mContext.showProgress();
        JsonDeviceUpdateObj obj = new JsonDeviceUpdateObj();
        obj.setDeviceId(posValue.getDeviceId().toString());
        obj.setName(name);
        API.deviceUpdate(this,obj);
    }

    /**
     *API
     * 寄出QRCode
     */
    private void callMailSendEmailDeviceIdAPI(String value) {
        mContext.showProgress();
        JsonMailSendEmailDeviceIdObj obj = new JsonMailSendEmailDeviceIdObj();
        obj.setEmail(value);
        API.mailSendEmailDeviceId(this,posValue.getDeviceId(),obj);
    }

    /**
     * API
     * 遠端開機
     */
    private void callOperatorDeviceStartAPI() {
        mContext.showProgress();
        JsonOperatorDeviceStartObj obj = new JsonOperatorDeviceStartObj();
        obj.setDeviceId(posValue.getDeviceId());
        API.operatorDeviceStart(this,obj);
    }

    private void onListener() {
        sales.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = API.deviceProfit() + posValue.getDeviceId();
                mContext.changeFragment(new SalesView(mContext,url));
            }
        });
        comboManage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.changeFragment(new ComboManageListView(mContext,posValue));
            }
        });
        bindCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBindCancelDialog();
            }
        });
        deviceChName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callDeviceChNameDialog();
            }
        });
        sendEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callSendEmailDialog();
            }
        });
        bootUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callOperatorDeviceStartAPI();
            }
        });
        groupChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mContext.changeFragment(new GroupChangeListView(mContext,posValue));
            }
        });
    }

    private void callBindCancelDialog() {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setMsg(getResources().getString(R.string.gdc_page_bind_cancel_ch_s));
        mAlertDialog.setEditHint(getString(R.string.user_password_s));
        mAlertDialog.setEditType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_PASSWORD);
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = mAlertDialog.getEditText();
                if (value != null && value.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return ;
                }
                callOperatorAuthConfirm(value);
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void callDeviceChNameDialog() {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setMsg(getResources().getString(R.string.gdc_page_setting_ch_device_name_s));
        mAlertDialog.setEditHint(this.title);
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = mAlertDialog.getEditText();
                if (value != null && value.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return ;
                }
                callDeviceUpdateAPI(value);
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void callSendEmailDialog() {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setMsg(getResources().getString(R.string.gdc_page_send_qr_s));
        mAlertDialog.setEditHint(getResources().getString(R.string.gdc_page_email_s));
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = mAlertDialog.getEditText();
                if (value != null && value.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return ;
                }

                if(!Util.isValidEmail(value)){
                    mContext.showDialog(getResources().getString(R.string.gdc_page_email_s));
                    return ;
                }

                callMailSendEmailDeviceIdAPI(value);
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }



    private void setShowView(JsonDeviceDetailedValue value) {
        this.value = value ;
        sn.setText(value.getSn());
        deviceName.setText(value.getDeviceName());
        groupName.setText(value.getGroupName());
        bindedDateTime.setText(Util.dateChange(mContext,value.getBindedDateTime()));
    }

    /**
     *API回傳
     */
    @Override
    public void onPostExecute(Object result , int missionCode) {
        mContext.dismissProgress();
        if(!ckeckError(result,mContext)){
            // 檢驗基礎傳輸錯誤
            return ;
        }
        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 8){
            // deviceDetailed
            JsonDeviceDetailedValue value = (JsonDeviceDetailedValue) data.getValue();
            setShowView(value);
        }

        if(missionCode == 18){
            // operatorAuthConfirm
            JsonOperatorAuthConfirmValue value = (JsonOperatorAuthConfirmValue) data.getValue();
            if(value.getVerifyResult()){
                callDeviceDeactiveAPI();
            } else {
                showDialog(mContext,getString(R.string.error_password_s));
            }
        }

        if(missionCode == 11){
            // deviceDeactive
            showDialog(mContext,getString(R.string.success_s));
            getFragmentManager().popBackStack();
        }

        if(missionCode == 21){
            // mailSendEmailDeviceId
            showDialog(mContext,getString(R.string.success_s));
        }

        if(missionCode == 22){
            // deviceUpdate
            showDialog(mContext,getString(R.string.success_s));
            callAPI();
        }

        if(missionCode == 28){
            // operatorDeviceStart
            showDialog(mContext,getString(R.string.game_start_s));
        }
    }

}
