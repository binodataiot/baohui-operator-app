package www.baohui.com.device;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.List;

import www.baohui.com.R;
import www.baohui.com.activity.MainActivity;
import www.baohui.com.adapter.DeviceListAdapter;
import www.baohui.com.model.req.JsonGroupCreateObj;
import www.baohui.com.model.req.JsonGroupDeleteObj;
import www.baohui.com.model.req.JsonGroupUpDateObj;
import www.baohui.com.model.res.JsonGroupListValue;
import www.baohui.com.model.res.JsonListObj;
import www.baohui.com.model.res.JsonResponseObj;
import www.baohui.com.service.API;
import www.baohui.com.service.BaseFragment;
import www.baohui.com.service.MAlertDialog;

/**
 * page
 * 機台總表頁面-群組
 */
@SuppressLint("ValidFragment")
public class DeviceListView extends BaseFragment {
    private MainActivity mContext;
    private View view;
    private DeviceListAdapter adapter;

    private ListView listView;
    private ImageView imageView;

    private  List<JsonGroupListValue> value;

    public DeviceListView(MainActivity context) {
        mContext = context ;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().setTitle(getResources().getString(R.string.group_list_s));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.device_list_view, container, false);
        findView();
        initData();
        onListener();
        return this.view;
    }

    private void findView() {
        listView = (ListView) this.view.findViewById(R.id.listView);
        imageView = (ImageView) this.view.findViewById(R.id.imageView);
    }

    private void initData() {
        callAPI();
    }

    /**
     *  API
     *  群組List
     */
    private void callAPI() {
        mContext.showProgress();
        API.groupList(this);
    }

    /**
     *API
     * 更新群組名稱
     */
    private void callUpDateAPI(String value, JsonGroupListValue posValue) {
        mContext.showProgress();
        JsonGroupUpDateObj obj = new JsonGroupUpDateObj();
        obj.setGroupName(value);
        obj.setGroupId(posValue.getGroupId().toString());
        API.groupUpDate(this,obj);
    }

    /**
     *API
     * 建立新群組
     */
    private void callCreateAPI(String value) {
        mContext.showProgress();
        JsonGroupCreateObj obj = new JsonGroupCreateObj();
        obj.setGroupName(value);
        API.groupCreate(this,obj);
    }

    /**
     *API
     * 刪除群組
     */
    private void callGroupDeleteAPI(JsonGroupListValue value) {
        mContext.showProgress();
        JsonGroupDeleteObj obj = new JsonGroupDeleteObj();
        obj.setGroupId(value.getGroupId().toString());
        API.groupDelete(this,obj);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_group_add_item, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_add:
                callAddDialog();
                return true;
            default:
                break;
        }
        return false;
    }

    private void onListener() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                JsonGroupListValue posValue = (JsonGroupListValue) adapter.getItem(position);
                mContext.changeFragment(new GroupListView(mContext, posValue));
            }
        });
        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                JsonGroupListValue posValue = (JsonGroupListValue) adapter.getItem(position);
                callUpDateDialog(posValue);
                return true;
            }
        });
    }

    private void callUpDateDialog(final JsonGroupListValue posValue) {
        String groupName = posValue.getGroupName();
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setEditHint(groupName);
        mAlertDialog.setMsg(getResources().getString(R.string.gdc_page_setting_ch_group_name_s));
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = mAlertDialog.getEditText();
                if (value != null && value.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return;
                } else {
                    callUpDateAPI(value,posValue);
                }
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void callAddDialog() {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        mAlertDialog.setEditHint(getResources().getString(R.string.gdc_page_setting_group_name_s));
        mAlertDialog.setMsg(getResources().getString(R.string.gdc_page_add_s));
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String value = mAlertDialog.getEditText();
                if (value != null && value.equals("")) {
                    mContext.showDialog(getString(R.string.please_in_key_s));
                    return ;
                } else {
                    callCreateAPI(value);
                }
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void callDeleteDialog(final JsonGroupListValue value) {
        final MAlertDialog mAlertDialog = new MAlertDialog(mContext);
        String name = value.getGroupName();
        mAlertDialog.setMsg(getResources().getString(R.string.gdc_page_ch_delete_s) + name + "?");
        mAlertDialog.showCancel(true);
        mAlertDialog.setOkOnListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callGroupDeleteAPI(value);
                mAlertDialog.dismiss();
            }
        });
        mAlertDialog.show();
    }

    private void setListView(List<JsonGroupListValue> value) {
        this.value = value;
        adapter = new DeviceListAdapter(mContext, this.value, deleteListener);
        listView.setAdapter(adapter);
        adapter.notifyDataSetChanged();
        imageView.setVisibility((adapter.getCount() != 0) ? View.GONE : View.VISIBLE);
    }

    private View.OnClickListener deleteListener = new View.OnClickListener(){

        @Override
        public void onClick(View v) {
            int pos = (int) v.getTag();
            callDeleteDialog(value.get(pos));
        }
    };

    /**
     * API回傳
     */
    public void onPostExecute(Object result , int missionCode) {
        mContext.dismissProgress();
        if(!ckeckError(result,mContext)){
            // 檢驗基礎傳輸錯誤
            return ;
        }

        JsonResponseObj data = (JsonResponseObj) result;
        if(missionCode == 5){
            // groupList
            JsonListObj jsonListObj = (JsonListObj) data.getValue();
            setListView(jsonListObj.getValue());
        }

        if(missionCode == 3){
            // groupCreate
            showDialog(mContext,getString(R.string.success_s));
            callAPI();
        }

        if(missionCode == 4){
            // groupUpdate
            showDialog(mContext,getString(R.string.success_s));
            callAPI();
        }

        if(missionCode == 24){
            // groupDelete
            showDialog(mContext,getString(R.string.success_s));
            callAPI();
        }

    }
}
